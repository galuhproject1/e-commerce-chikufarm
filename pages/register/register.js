import "tailwindcss/tailwind.css";
import axios from "axios";
import React, { useState } from "react";
import { useRouter } from "next/router";

export default function Register() {
  const [email, setEmail] = useState("");
  const [nama, setNama] = useState("");
  const [password, setPassword] = useState("");
  const [no_hp, setNo_Hp] = useState("");
  const [alamat, setAlamat] = useState("");
  const [roles, setRoles] = useState("customer");

  const router = useRouter();
  const registerData = async () => {
    const valueFrom = {
      email: email,
      nama: nama,
      password: password,
      no_hp: no_hp,
      alamat: alamat,
      roles: roles,
    };
    const res = await axios.post("http://localhost:3222/users", valueFrom);
    console.log(res);
    if (res.status == 201 || res.status == 200) {
      router.push("/login");
    }
  };

  const onChangeEmail = (e) => {
    const value = e.target.value;
    setEmail(value);
  };
  const onChangeNama = (e) => {
    const value = e.target.value;
    setNama(value);
  };
  const onChangePassword = (e) => {
    const value = e.target.value;
    setPassword(value);
  };
  const onChangeNo_Hp = (e) => {
    const value = e.target.value;
    setNo_Hp(value);
  };
  const onChangeAlamat = (e) => {
    const value = e.target.value;

    setAlamat(value);
  };

  const onFormSubmit = (e) => {
    e.preventDefault();
    console.log("ok");
  };

  return (
    <>
      <div className="flex w-full flex-1 flex-col items-center justify-center px-20 pt-20 text-center">
        <div className="bg-[#A6EEC7] rounded-2xl shadow-2xl flex w-2/3 max-w-4xl">
          {/* login section */}
          <div className="w-2/5 bg-[#56B280] text-[#76323F] rounded-tl-2xl rounded-bl-2xl py-36 px-12">
            <h2 className="text-2xl font-bold mb-2 text-[#76323F]">
              Dear Guest!
            </h2>
            <p className="mb-10">let's join us</p>
            <a
              href="/login"
              className="border-2 bg-[#76323F] border-[#76323F] text-[#fff] font-semibold rounded-full px-8 py-1"
            >
              LOGIN
            </a>
          </div>

          {/* Section signup */}
          <div className="w-3/5 p-5">
            <div className="flex flex-col items-center bg-[#A6EEC7]">
              <div className="py-5">
                <h2 className="text-3xl font-bold text-[#76323F]">
                  Create Guest Account
                </h2>
              </div>
              <form onSubmit={onFormSubmit}>
                <div className="bg-white w-80 p-2 rounded-full shadow-sm flex items-center mb-3">
                  <input
                    type="text"
                    name="fullname"
                    placeholder="fullname"
                    className="ml-2 outline-none text-sm text [#565656] flex-1"
                    // value={fullnama}
                    onChange={onChangeNama}
                  />
                </div>
                <div className="bg-white w-80 p-2 rounded-full shadow-sm flex items-center mb-3">
                  <input
                    type="text"
                    name="username"
                    placeholder="alamat"
                    className="ml-2 outline-none text-sm text [#565656] flex-1"
                    // value={alamat}
                    onChange={onChangeAlamat}
                  />
                </div>
                <div className="bg-white w-80 p-2 rounded-full shadow-sm flex items-center mb-3">
                  <input
                    type="text"
                    name="phone"
                    placeholder="phone"
                    className="ml-2 outline-none text-sm text [#565656] flex-1"
                    // value={no_hp}
                    onChange={onChangeNo_Hp}
                  />
                </div>
                <div className="bg-white w-80 p-2 rounded-full shadow-sm flex items-center mb-3">
                  <input
                    type="text"
                    name="email"
                    placeholder="email"
                    className="ml-2 outline-none text-sm text [#565656] flex-1"
                    // value={email}
                    onChange={onChangeEmail}
                  />
                </div>
                <div className="bg-white w-80 p-2 rounded-full shadow-sm flex items-center mb-3">
                  <input
                    type="password"
                    name="password"
                    placeholder="password"
                    className="ml-2 outline-none text-sm text [#565656] flex-1"
                    // value={password}
                    onChange={onChangePassword}
                  />
                </div>
                <button
                  type="submit"
                  onClick={registerData}
                  className="border-2 bg-[#F9EAE1] border-[#F9EAE1] font-bold rounded-full w-80  p-2 inline-block"
                >
                  SIGN UP
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
