import Head from "next/head";
import Image from "next/image";
import NavigasiTiga from "./components/navbar3";
import NavbarCustomer from "./components/navbarCustomer";
import styles from "../styles/Home.module.css";
import Header from "./components/header";
import "antd/dist/antd.css";
import ProductHome from "./components/product";
import OurService from "./components/ourService";
import MoreProduct from "./components/moreProduct";
import Footer from "./components/footer";

// const isLogin = () => {
//   if (window !== undefined) {
//     const token = localStorage.getItem("token_customer");
//     if (token) {
//       return true;
//     } else {
//       return false;
//     }
//   }
// };

export default function Home() {
  return (
    <>
      {/* {!isLogin() ? <NavigasiTiga /> : <NavbarCustomer />} */}
      <NavigasiTiga />
      <Header />
      <ProductHome />
      <MoreProduct />
      <OurService />
      <Footer />
    </>
  );
}
