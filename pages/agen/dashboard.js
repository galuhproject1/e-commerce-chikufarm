import KontenDashboard from "./konten/kontenDashboard";
import MainLayout from "./Layout/MainLayout";
function DashboardAdmin() {
  return (
    <>
      <MainLayout>
        <KontenDashboard />
      </MainLayout>
    </>
  );
}

export default DashboardAdmin;
