import KontenPemesanan from "./konten/kontenPemesanan";
import MainLayout from "./Layout/MainLayout";

function AgenAdmin() {
  return (
    <>
      <MainLayout>
        <KontenPemesanan />
      </MainLayout>
    </>
  );
}

export default AgenAdmin;
