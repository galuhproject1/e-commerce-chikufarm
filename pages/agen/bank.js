import KontenBank from "./konten/kontenBank";
import MainLayout from "./Layout/MainLayout";

function Bank() {
  return (
    <>
      <MainLayout>
        <KontenBank />
      </MainLayout>
    </>
  );
}

export default Bank;
