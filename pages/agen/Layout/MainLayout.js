import { Layout, ConfigProvider } from "antd";
import NavbarAgen from "./NavbarAgen";
import Sidebar from "./SidebarAgen";
import "antd/dist/antd.variable.css";
import "tailwindcss/tailwind.css";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
const { Footer } = Layout;
ConfigProvider.config({
  theme: {
    primaryColor: "#56B280",
  },
});

function MainLayout({ children }) {
  const router = useRouter();

  useEffect(() => {
    const getToken = localStorage.getItem("token_agen");
    if (!getToken) {
      message.error("Anda harus login dahulu");
      router.push("/login");
    }
  }, []);

  return (
    <Layout hasSider>
      <Sidebar />
      <Layout>
        <NavbarAgen />
        <Layout style={{ backgroundColor: "#A6EEC7" }}>{children}</Layout>
      </Layout>
    </Layout>
  );
}

export default MainLayout;
