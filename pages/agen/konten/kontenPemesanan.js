import {
  Layout,
  Header,
  Row,
  Col,
  Card,
  Space,
  Table,
  Tag,
  Button,
  Modal,
  Checkbox,
  Form,
  Input,
  Tooltip,
} from "antd";
import { PlusOutlined, EyeOutlined } from "@ant-design/icons";
import React, { useState, useEffect } from "react";
import Image1 from "../../image/paymentconfirm.jpeg";
import Image from "next/image";
import { render } from "react-dom";
import axios from "axios";
import { useRouter } from "next/router";
import jwt_decode from "jwt-decode";
import Link from "next/link";
const { Content } = Layout;

export default function KontenAgen() {
  const [dataPemesanan, setDatapemesanan] = useState([]);
  async function getData() {
    try {
      await axios
        .get("http://localhost:3222/transaksi_pembeli", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_agen")}`,
          },
        })
        .then((res) => {
          setDatapemesanan(res.data.data);
        });
    } catch (error) {}
  }

  useEffect(() => {
    getData();
  }, []);
  // console.log(dataPemesanan[0].pembeli.nama);

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const columns = [
    {
      title: "Nama",
      dataIndex: "agen",
      key: "agen",
    },
    {
      title: "Produk",
      dataIndex: "produk",
      key: "produk",
    },
    {
      title: "Jumlah Pembayaran",
      dataIndex: "total_bayar",
      key: "total_bayar",
    },
    {
      title: "Tanggal Pembayaran",
      dataIndex: "tanggal",
      key: "tanggal",
    },
    {
      title: "Bukti",
      dataIndex: "bukti_bayar",
      key: "bukti_bayar",
      render: (_, record) => (
        <>
          <Button
            type="primary"
            onClick={showModal}
            style={{
              overflow: "hidden",
              background: "white",
              borderColor: "white",
            }}
          >
            <p className="underline text-black">
              Bukti <i className="fa-solid fa-eye underline"></i>
            </p>
          </Button>
          <Modal
            title="Bukti Pembayaran"
            visible={isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
            style={{ width: "200px", height: "200px" }}
          >
            <Image src={Image1} />
          </Modal>
        </>
      ),
    },

    {
      title: "Konfirmasi",
      key: "operation",
      fixed: "right",
      width: 100,
      render: () => (
        <a className="bg-emerald-500 px-5 py-1 rounded-md text-white">
          Confirm
        </a>
      ),
    },
  ];

  const temp = dataPemesanan.map((values) => {
    console.log(values);
    const data = {
      agen: values.pembeli.nama,
      produk: values.produkAgen.nama_produk,
      total_bayar: values.total_bayar,
      tanggal: values.tanggal,
    };
    return data;
  });
  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="bg-white rounded-md align-middle my-10 mx-12 pb-5">
      <Content style={{ marginTop: "50px" }}>
        <Row justify="center" align="middle">
          <Col span={22}>
            <Table columns={columns} dataSource={temp} />
          </Col>
        </Row>
      </Content>
    </div>
  );
}
