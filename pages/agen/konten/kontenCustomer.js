import {
  Layout,
  Header,
  Row,
  Col,
  Card,
  Space,
  Table,
  Tag,
  Button,
  Modal,
  Checkbox,
  Form,
  Input,
  Tooltip,
  Select,
  Option,
} from "antd";
import {
  PlusOutlined,
  PrinterOutlined,
  PoweroffOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import React, { useEffect, useState } from "react";
import { render } from "react-dom";
import Link from "next/link";
import axios from "axios";
const { Content } = Layout;

export default function KontenCustomer() {
  const [dataUser, setDatauser] = useState([]);
  async function getData() {
    try {
      await axios.get("http://localhost:3222/users?type=2").then((res) => {
        setDatauser(res.data.data);
        console.log(res);
      });
    } catch (error) {}
  }
  // console.log(dataUser);

  useEffect(() => {
    getData();
  }, []);

  const columns = [
    {
      title: "Full Name",
      dataIndex: "nama",
      key: "nama",
    },
    {
      title: "Alamat",
      dataIndex: "alamat",
      key: "almaat",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Phone",
      dataIndex: "no_hp",
      key: "no_hp",
    },

    {
      title: "Role",
      dataIndex: "",
      key: "role",
      render: () => <a>Customer</a>,
    },

    // {
    //   title: "Action",
    //   key: "action",
    //   render: (_, record) => (
    //     <Space size="middle">
    //       <Link href={`/admin/${record.name}`}>
    //         <Tooltip placement="left" title="Detail">
    //           <Button
    //             style={{ color: "#517BEA", borderColor: "#4ade80" }}
    //             icon={<i className="fa-solid fa-pencil"></i>}
    //           ></Button>
    //         </Tooltip>
    //       </Link>
    //       <Link href={`/${record.deleteUser}`}>
    //         <Tooltip placement="right" title="Delete">
    //           <Button
    //             type="danger"
    //             icon={<i className="fa-solid fa-trash-can"></i>}
    //             danger={true}
    //           ></Button>
    //         </Tooltip>
    //       </Link>
    //     </Space>
    //   ),
    // },
  ];

  const temp = dataUser.map((values) => {
    const data = {
      nama: values.nama,
      alamat: values.alamat,
      email: values.email,
      no_hp: values.no_hp,
    };
    return data;
  });

  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const handlePrint = () => {
    window.print();
  };
  const { Option } = Select;

  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };
  return (
    <div className="bg-white rounded-md align-middle my-10 mx-12 pb-5">
      {/* <Layout style={{ backgroundColor: "#A6EEC7" }}> */}
      <Content style={{ marginTop: "10px" }}>
        <Row justify="end">
          <Col span={22} pull={1} className="bg-white">
            <Row justify="end" className="mt-5 mr-5">
              <Col span={3} pull={2}>
                <Button
                  onClick={handlePrint}
                  type="primary"
                  style={{
                    marginBottom: "20px",
                    marginLeft: "50px",
                    marginBottom: "10px",
                    width: "150px",
                  }}
                >
                  <PrinterOutlined />
                  Print
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>

        <Row justify="center" align="middle">
          <Col span={22}>
            <Table columns={columns} dataSource={temp} />
          </Col>
        </Row>
      </Content>
      {/* </Layout> */}
    </div>
  );
}
