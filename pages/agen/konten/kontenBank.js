import {
  Layout,
  Header,
  Row,
  Col,
  Card,
  Space,
  Table,
  Tag,
  Button,
  Modal,
  Checkbox,
  Form,
  Input,
  Tooltip,
  Select,
  Option,
} from "antd";
import {
  PlusOutlined,
  PoweroffOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import Link from "next/link";
import axios from "axios";
import jwt_decode from "jwt-decode";
import { useRouter } from "next/router";
import { data } from "autoprefixer";
const { Content } = Layout;

function getColumns(deleteModal, updateModal) {
  return [
    {
      title: "Bank",
      dataIndex: "nama_bank",
      key: "nama_bank",
    },
    {
      title: "Nama Akun Bank",
      dataIndex: "nama_akun_bank",
      key: "nama_akun_bank",
    },
    {
      title: "Rekening",
      dataIndex: "no_rekening",
      key: "no_rekening",
    },

    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Tooltip placement="left" title="edit">
            <Button
              onClick={() => updateModal(record)}
              style={{ color: "#517BEA", borderColor: "#4ade80" }}
              icon={<i className="fa-solid fa-pencil"></i>}
            ></Button>
          </Tooltip>

          <Tooltip placement="right" title="Delete">
            <Button
              type="danger"
              onClick={() => deleteModal(record)}
              icon={<i className="fa-solid fa-trash-can"></i>}
              danger={true}
            ></Button>
          </Tooltip>
        </Space>
      ),
    },
  ];
}

export default function KontenBank() {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [loadingDua, setLoadingDua] = useState(false);
  // state modal delete
  const [visible, setVisible] = useState(false);
  const [modalText, setModalText] = useState("Content of the modal");
  const [modalTaskId, setModalTaskId] = useState("");
  //state modal update
  const [visibleDua, setVisibleDua] = useState(false);
  const [modalTextDua, setModalTextDua] = useState("Content of the modal");
  const [modalTaskIdDua, setModalTaskIdDua] = useState("");
  // const [foto, setFoto] = useState('')

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    console.log("Clicked cancel button");
    setVisible(false);
    setVisibleDua(false);
    setIsModalVisible(false);
  };
  // modal delete
  const deleteModal = (record) => {
    if (record) {
      setModalTaskId(record);
      setVisible(true);
    } else {
      setVisible(false);
    }
  };
  const handleOkModalDelete = () => {
    axios
      .delete(`http://localhost:3222/bank/${modalTaskId.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token_admin")}`,
        },
      })
      .then((res) => {
        console.log(res);
      });
    setModalText("The modal will be closed after one second");
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 1000);
    // location.reload()
  };

  // modal update
  const updateModal = (record) => {
    console.log(record, "ini dari Update Modal");
    if (record) {
      setModalTaskIdDua(record.id);
      setVisibleDua(true);
      form.setFieldsValue({
        nama: record.nama,
        email: record.email,
        no_hp: record.no_hp,
        alamat: record.alamat,
      });
    } else {
      setVisibleDua(false);
    }
  };

  const handleOkModalUpdate = async (record) => {
    // console.log(modalTaskIdDua, "ini record");
    try {
      const data = await form.getFieldsValue();
      console.log(data);

      await axios
        .put(`http://localhost:3222/bank/${modalTaskIdDua}`, data, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_admin")}`,
            "content-type": "application/json",
          },
        })
        .then((res) => {
          console.log(res, "Berhasil edit");
        });
      setModalTextDua("The modal will be closed after one seconds");
      setConfirmLoading(true);
      setTimeout(() => {
        setVisibleDua(false);
        setConfirmLoading(false);
      }, 1000);
      // location.reload()
    } catch (error) {
      console.log(error.message);
    }
  };

  // get data ke tabel
  const [dataBank, setDataBank] = useState([]);
  async function getDataBank() {
    try {
      await axios
        .get("http://localhost:3222/bank", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_agen")}`,
            "content-type": "application/json",
          },
        })
        .then((res) => {
          setDataBank(res.data.data);
          //   console.log(res.data.data[0]?.nama_bank);
          console.log(res.data.data);
        });
    } catch (error) {}
  }

  useEffect(() => {
    getDataBank();
  }, []);

  const [email, setEmail] = useState("");
  const [nama, setNama] = useState("");
  const [password, setPassword] = useState("");
  const [no_hp, setNo_Hp] = useState("");

  const router = useRouter();

  const temp = dataBank.map((values) => {
    const data = {
      nama_bank: values.nama_bank,
      nama_akun_bank: values.nama_akun_bank,
      no_rekening: values.no_rekening,
    };
    return data;
  });

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const { Option } = Select;

  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };
  const role = "agen";

  const onFinish = async (values) => {
    try {
      const data = {
        nama: values.fullname,
        harga: values.harga,
        deskripsi: values.deskripsi,
        stok: values.stok,
      };
      const response = await axios.post("http://localhost:3222/bank", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token_agen")}`,
        },
      });
      console.log(response);
    } catch (error) {}
  };

  return (
    <div className="bg-white rounded-md align-middle my-10 mx-12 pb-5">
      {/* <Layout style={{ backgroundColor: "#A6EEC7" }}> */}
      <Content style={{ marginTop: "10px" }}>
        <Row justify="end">
          <Col span={22} pull={1} className="bg-white">
            <Row justify="end" className="mt-5 mr-5">
              <Col span={3} pull={2}>
                <Button
                  type="primary"
                  onClick={showModal}
                  style={{ marginBottom: "20px", marginLeft: "90px" }}
                >
                  <PlusOutlined />
                  Add Bank
                </Button>

                <Modal
                  title="Add Bank"
                  visible={isModalVisible}
                  onOk={handleOk}
                  onCancel={handleCancel}
                  footer={false}
                >
                  <Form
                    name="basic"
                    labelCol={{}}
                    wrapperCol={{}}
                    initialValues={{
                      remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                  >
                    <h1 className="text-start">Nama</h1>
                    <Form.Item name="fullname">
                      <Input placeholder="Fullname" />
                    </Form.Item>
                    <h1 className="text-start">Bank</h1>
                    <Form.Item name="password">
                      <Input.Password placeholder="Bank" />
                    </Form.Item>
                    <h1 className="text-start">Nama Akun Bank</h1>
                    <Form.Item name="alamat">
                      <Input placeholder="Nama Akun Bank" />
                    </Form.Item>
                    <h1 className="text-start">No Rekening</h1>
                    <Form.Item name="email">
                      <Input placeholder="No Rekening" />
                    </Form.Item>

                    <Form.Item>
                      <Row>
                        <Col span={6} push={5} offset={3}>
                          <Button
                            type="primary"
                            htmlType="cancel"
                            onClick={handleCancel}
                          >
                            Cancel
                          </Button>
                        </Col>
                        <Col span={4} offset={4}>
                          <Button
                            type="primary"
                            htmlType="submit"
                            onClick={handleOk}
                          >
                            Submit
                          </Button>
                        </Col>
                      </Row>
                    </Form.Item>
                  </Form>
                </Modal>
              </Col>
            </Row>
          </Col>
        </Row>

        <Row justify="center" align="middle">
          <Col span={22}>
            <Table
              columns={getColumns(deleteModal, updateModal)}
              dataSource={temp}
            />
            <Modal
              name="basic"
              title="Konfirmasi Penghapusan"
              visible={visible}
              onOk={handleOkModalDelete}
              confirmLoading={confirmLoading}
              onCancel={handleCancel}
            >
              <p className="text-emerald-500">
                Apakah anda yakin akan meghapus user yang memiliki nama
              </p>
              <p className="text-emerald-500">
                {JSON.stringify(modalTaskId.nama)}
              </p>
            </Modal>
            <Modal
              title="Konfirmasi Update Data"
              visible={visibleDua}
              onOk={handleOkModalUpdate}
              confirmLoading={confirmLoading}
              onCancel={handleCancel}
              // footer={false}
            >
              <Form
                form={form}
                name="basic"
                labelCol={{}}
                wrapperCol={{}}
                initialValues={{
                  remember: true,
                }}
                onFinish={handleOkModalUpdate}
                // onFinishFailed={onFinishFailed}
                // autoComplete="off"
              >
                <h1 className="text-start">Nama</h1>
                <Form.Item name="nama">
                  <Input />
                </Form.Item>

                <h1 className="text-start">Bank</h1>
                <Form.Item name="alamat">
                  <Input />
                </Form.Item>
                <h1 className="text-start">Nama akun bank</h1>
                <Form.Item name="email">
                  <Input />
                </Form.Item>

                <h1 className="text-start">No rekening</h1>
                <Form.Item name="no_hp">
                  <Input />
                </Form.Item>
                {/* <Form.Item>
                    <Row>
                      <Col span={6} push={5} offset={3}>
                        <Button
                          type="primary"
                          htmlType="cancel"
                          onClick={handleCancel}
                        >
                          Cancel
                        </Button>
                      </Col>
                      <Col span={4} offset={4}>
                        <Button
                          type="primary"
                          htmlType="submit"
                          onClick={handleOkModalUpdate}
                        >
                          Submit
                        </Button>
                      </Col>
                    </Row>
                  </Form.Item> */}
              </Form>
            </Modal>
          </Col>
        </Row>
      </Content>
      {/* </Layout> */}
    </div>
  );
}
