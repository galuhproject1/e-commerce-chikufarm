import {
  Layout,
  Header,
  Row,
  Col,
  Card,
  Space,
  Table,
  Tag,
  Button,
  Modal,
  Checkbox,
  Form,
  Input,
  Tooltip,
  message,
  Upload,
  Select,
} from "antd";
import {
  PlusOutlined,
  PrinterOutlined,
  PoweroffOutlined,
  DeleteOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import React, { useState, useEffect, useLayoutEffect } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";
import { useRouter } from "next/router";
import { render } from "react-dom";
import Link from "next/link";
const { Content } = Layout;

const columns = [
  {
    title: "Nama Pemesan",
    dataIndex: "agen",
    key: "agen",
  },
  {
    title: "Produk",
    dataIndex: "produk",
    key: "produk",
  },

  {
    title: "Harga Total",
    dataIndex: "total_bayar",
    key: "total_bayar",
  },

  {
    title: "Action",
    key: "action",
    render: (_, record) => (
      <Space size="middle">
        <Link href={`/admin/${record.name}`}>
          <Tooltip placement="left" title="Edit">
            <Button
              style={{ color: "#517BEA", borderColor: "#4ade80" }}
              icon={<i className="fa-solid fa-pencil"></i>}
            ></Button>
          </Tooltip>
        </Link>
        <Link href={`/${record.deleteUser}`}>
          <Tooltip placement="right" title="Delete">
            <Button
              type="danger"
              icon={<i className="fa-solid fa-trash-can"></i>}
              danger={true}
            ></Button>
          </Tooltip>
        </Link>
      </Space>
    ),
  },
];

function Kontenkeranjang() {
  const [form] = Form.useForm();
  const [dataPemesanan, setDatapemesanan] = useState([]);
  async function getData() {
    try {
      await axios
        .get("http://localhost:3222/transaksi_agen", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_agen")}`,
          },
        })
        .then((res) => {
          setDatapemesanan(res.data.data);
        });
    } catch (error) {}
  }
  useEffect(() => {
    getData();
  }, []);
  const temp = dataPemesanan.map((values) => {
    console.log(values);
    const data = {
      agen: values.agen.nama,
      produk: values.produkPusat.nama,
      total_bayar: values.total_bayar,
    };
    return data;
  });

  const handleOkModalUpdate = async (record) => {
    // console.log(modalTaskIdDua, "ini record");
    try {
      const data = await form.getFieldsValue();
      console.log(data);
      await axios
        .put(`http://localhost:3222/users/${modalTaskIdDua}`, data, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_admin")}`,
            "content-type": "application/json",
          },
        })
        .then((res) => {
          console.log(res, "Berhasil edit");
        });
      setModalTextDua("The modal will be closed after one seconds");
      setConfirmLoading(true);
      setTimeout(() => {
        setVisibleDua(false);
        setConfirmLoading(false);
      }, 1000);
      // location.reload()
    } catch (error) {
      console.log(error.message);
    }
  };

  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const onChangeImage = (e) => {
    const value = e.target.files[0];
  };

  const { Option } = Select;

  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };

  return (
    <div>
      <Layout style={{ backgroundColor: "#A6EEC7" }}>
        <div className="bg-white rounded-md align-middle my-10 mx-12 pb-5">
          <Content style={{ marginTop: "10px" }}>
            <Row justify="end">
              <Col span={22} pull={1} className="bg-white">
                <Row justify="end" className="mt-5 mr-5">
                  <Col span={3} pull={2}>
                    <Button
                      type="primary"
                      onClick={showModal}
                      style={{ marginBottom: "20px", marginLeft: "90px" }}
                    >
                      <PlusOutlined />
                      Add Cart
                    </Button>

                    <Modal
                      title="Add Cart"
                      visible={isModalVisible}
                      onOk={handleOk}
                      onCancel={handleCancel}
                      footer={[
                        <>
                          <Button
                            type="primary"
                            key="back"
                            onClick={handleCancel}
                            style={{ backgroundColor: "white", color: "black" }}
                          >
                            Cancel
                          </Button>
                          <Button
                            key="submit"
                            onClick={handleOk}
                            style={{ backgroundColor: "#A6EEC7" }}
                          >
                            Submit
                          </Button>
                        </>,
                      ]}
                    >
                      <Form
                        name="basic"
                        initialValues={{
                          remember: true,
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                      >
                        <Form.Item
                          rules={[
                            {
                              required: true,
                              message: "Please input Name of profuct!",
                            },
                          ]}
                        >
                          <h1 className="text-start">Product</h1>
                          <Select
                            defaultValue="telur"
                            style={{
                              width: 120,
                            }}
                            onChange={handleChange}
                          >
                            <Option value="telurAyam">Telur ayam</Option>
                            <Option value="telurBebek">Telur bebek</Option>
                            <Option value="bibitAyam">Bibit ayam</Option>
                            <Option value="bibitBebek">Bibit bebek</Option>
                            <Option value="ayamPetelur">Ayam petelur</Option>
                            <Option value="bebekPetelur">Bebebk petelur</Option>
                            <Option value="ayamAfkir">Ayam Afkir</Option>
                            <Option value="bebekAfkir">bebekAfkir</Option>
                          </Select>
                        </Form.Item>
                        <Form.Item
                          rules={[
                            {
                              required: true,
                              message: "Please input ID!",
                            },
                          ]}
                        >
                          <h1 className="text-start">Nama Pemesan</h1>
                          <Input placeholder="Nama Pemesan" />
                        </Form.Item>
                        <Form.Item
                          rules={[
                            {
                              required: true,
                              message: "Please input total!",
                            },
                          ]}
                        >
                          <h1 className="text-start">Jumlah</h1>
                          <Input placeholder="Jumlah" />
                        </Form.Item>

                        <Form.Item>
                          <h1 className="text-start">Pesan</h1>
                          <Input.TextArea placeholder="Pesan" />
                        </Form.Item>
                      </Form>
                    </Modal>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row justify="center" align="middle">
              <Col span={22}>
                <Table
                  columns={columns}
                  dataSource={temp}
                  bordered
                  pagination={false}
                  style={{ marginBottom: "10px" }}
                />
              </Col>
              <Col span={22}>
                <Card
                  style={{
                    width: 887,
                    height: 58,
                    backgroundColor: "#A6EEC7",
                  }}
                >
                  <Row>
                    <Col className="text-2xl -mt-3">
                      <i className="fa-solid fa-credit-card text-emerald-900"></i>
                    </Col>
                    <Col>
                      <p className="text-md -mt-2 mx-5 font-bold">Transfer</p>
                    </Col>
                  </Row>
                </Card>
                <Card
                  style={{
                    width: 887,
                    height: "auto",
                  }}
                >
                  <Form
                    name="basic"
                    initialValues={{
                      remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                  >
                    <Form.Item>
                      <Select
                        defaultValue="BRI"
                        style={{
                          width: 837,
                        }}
                        onChange={handleChange}
                      >
                        <Option value="BRI">BRI - 6500 70xx xxx xxxx</Option>
                      </Select>
                    </Form.Item>

                    <Form.Item
                      rules={[
                        {
                          required: true,
                        },
                      ]}
                    >
                      <div>
                        <input
                          id="img"
                          style={{ display: "none" }}
                          type="file"
                          accept="image/*"
                          className="form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 
                    bg-white bg-clip-padding  border-emerald-400 rounded transition 
                    ease-in-out m-0 focus:text-emerald-700 focus:bg-white focus:border-emerald-600 focus:outline-none"
                          onChange={onChangeImage}
                        />

                        <button className="w-full inline-block px-6 py-2 border-2 border-emerald-500 text-emerald-500 font-medium text-xs leading-tight uppercase rounded-sm hover:bg-black hover:bg-opacity-5 focus:outline-none focus:ring-0 transition duration-150 ease-in-out">
                          <label htmlFor="img">
                            <UploadOutlined /> upload bukti transfer{" "}
                          </label>
                        </button>
                      </div>
                    </Form.Item>

                    <Form.Item
                      wrapperCol={{
                        offset: 10,
                        span: 14,
                      }}
                    >
                      <Button type="primary" htmlType="submit">
                        Konfirm
                      </Button>
                    </Form.Item>
                  </Form>
                </Card>
              </Col>
            </Row>
          </Content>
        </div>
      </Layout>
    </div>
  );
}
export default Kontenkeranjang;
