import { Col, Row, Divider, Card, Button } from "antd";
import { PrinterOutlined } from "@ant-design/icons";
import * as React from "react";
import { AreaChart, Area } from "recharts";

const data = [
  {
    name: "Juli",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Page F",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];

function KontenDashboard() {
  return (
    <div className="mt-8">
      <Row justify="space-around" className="text-center">
        <Col span={6} offset={1} pull={1} className="bg-white p-5 rounded-xl">
          <div className="text-sm text-black">Total Produk</div>
          <div className="text-lg font-bold text-bold">3</div>
        </Col>
        <Col span={6} offset={1} pull={1} className="bg-white p-5 rounded-xl">
          <div className="text-sm text-black">Total Pembelian</div>
          <div className="text-lg font-bold text-bold">3</div>
        </Col>
        <Col span={6} offset={1} pull={1} className="bg-white p-5 rounded-xl">
          <div className="text-sm text-black">Jumlah Pemasukan</div>
          <div className="text-lg font-bold text-bold">1.000.000</div>
        </Col>
        {/* <Col span={4} offset={1} pull={1} className="bg-white p-5 rounded-xl">
          col-4
        </Col> */}
      </Row>

      <Row justify="space-around" className="text-center mt-5">
        <Col span={10} offset={1} pull={1}>
          <Card
            bordered={false}
            style={{
              backgroundColor: "white",
              borderRadius: "10px",
              height: "206px",
            }}
          >
            <div className="text-sm">Progress Pembelian</div>
            <div>
              <AreaChart
                width={400}
                height={150}
                data={data}
                margin={{
                  top: 5,
                  right: 0,
                  left: 0,
                  bottom: 5,
                }}
              >
                <Area
                  type="monotone"
                  dataKey="uv"
                  stroke="#76323F"
                  fill="#A6EEC7"
                />
              </AreaChart>
            </div>
          </Card>
        </Col>

        <Col span={10} offset={1}>
          <Row gutter={[8, 24]}>
            <Col span={10} offset={1}>
              <Card
                bordered={false}
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  width: "96%",
                  height: "100%",
                }}
              >
                <div className="text-sm text-black">Penjualan Bulan Lalu</div>
                <div className="text-lg font-bold text-bold">30</div>
              </Card>
            </Col>
            <Col span={10} offset={1}>
              <Card
                bordered={false}
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  width: "96%",
                  height: "100%",
                }}
              >
                <div className="text-sm text-black">Penjualan Bulan Ini</div>
                <div className="text-lg font-bold text-bold">50</div>
              </Card>
            </Col>
            <Col span={21} offset={1}>
              <Card
                bordered={false}
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  width: "98%",
                  height: "115%",
                }}
              >
                <p>Produk</p>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>

      <Row justify="space-around" className="mt-5">
        <Col span={10} offset={1} pull={1}>
          <Button
            style={{
              width: 450,
              height: 40,
              padding: "5px",
              backgroundColor: "#56B280",
              border: "10px",
              color: "white",
              fontSize: "20px",
            }}
          >
            Print Laporan <PrinterOutlined />
          </Button>
        </Col>
        <Col span={10} offset={1}></Col>
      </Row>
    </div>
  );
}

export default KontenDashboard;
