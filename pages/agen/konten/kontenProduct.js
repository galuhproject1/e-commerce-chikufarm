import {
  Layout,
  Header,
  Row,
  Col,
  Card,
  Space,
  Table,
  Tag,
  Button,
  Modal,
  Checkbox,
  Form,
  Input,
  Tooltip,
  message,
  Upload,
} from "antd";
import {
  PlusOutlined,
  PrinterOutlined,
  PoweroffOutlined,
  DeleteOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import Link from "next/link";
import axios from "axios";
import jwt_decode from "jwt-decode";
import { useRouter } from "next/router";
import { useForm } from "antd/lib/form/Form";
const { Content } = Layout;

function getColumns(updateModal, deleteModal) {
  return [
    {
      title: "Nama agen",
      key: "nama",
      render: (value) => value.agen.nama,
    },
    {
      title: "Produk",
      dataIndex: "nama_produk",
      key: "nama_produk",
    },

    {
      title: "Deskripsi",
      dataIndex: "deskripsi",
      key: "deskripsi",
    },
    {
      title: "Harga",
      dataIndex: "harga",
      key: "harga",
    },
    {
      title: "Stok",
      dataIndex: "stok",
      key: "stok",
    },

    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Tooltip placement="left" title="edit">
            <Button
              onClick={() => updateModal(record)}
              style={{ color: "#517BEA", borderColor: "#4ade80" }}
              icon={<i className="fa-solid fa-pencil"></i>}
            ></Button>
          </Tooltip>
          <Tooltip placement="right" title="Delete">
            <Button
              type="danger"
              onClick={() => deleteModal(record)}
              icon={<i className="fa-solid fa-trash-can"></i>}
              danger={true}
            ></Button>
          </Tooltip>
        </Space>
      ),
    },
  ];
}

export default function KontenProduk() {
  const [dataProduk, setDataproduk] = useState([]);
  const router = useRouter();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [loadingDua, setLoadingDua] = useState(false);
  // state modal delete
  const [visible, setVisible] = useState(false);
  const [modalText, setModalText] = useState("Content of the modal");
  const [modalTaskId, setModalTaskId] = useState("");
  //state modal update
  const [visibleDua, setVisibleDua] = useState(false);
  const [modalTextDua, setModalTextDua] = useState("Content of the modal");
  const [modalTaskIdDua, setModalTaskIdDua] = useState("");
  async function getData() {
    try {
      await axios
        .get("http://localhost:3222/produk_agen", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_agen")}`,
          },
        })
        .then((res) => {
          setDataproduk(res.data.data);
        });
    } catch (error) {}
  }
  // console.log(dataProduk);

  useEffect(() => {
    getData();
  }, []);

  // modal update
  const updateModal = (record) => {
    console.log(record, "ini dari Update Modal");
    if (record) {
      setModalTaskIdDua(record.id);
      setVisibleDua(true);
      form.setFieldsValue({
        nama_produk: record.nama_produk,
        harga: record.harga,
        deskripsi: record.deskripsi,
      });
    } else {
      setVisibleDua(false);
    }
  };

  const handleOkModalUpdate = async (record) => {
    console.log(modalTaskIdDua, "ini record");
    try {
      const data = await form.getFieldsValue();
      console.log(data);

      await axios
        .put(`http://localhost:3222/produk_agen/${modalTaskIdDua}`, data, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_agen")}`,
            "content-type": "application/json",
          },
        })
        .then((res) => {
          console.log(res, "Berhasil edit");
        });
      setModalTextDua("The modal will be closed after one seconds");
      setConfirmLoading(true);
      setTimeout(() => {
        setVisibleDua(false);
        setConfirmLoading(false);
      }, 1000);
      // location.reload()
      getData();
    } catch (error) {
      console.log(error.message);
    }
  };

  // modal delete
  const deleteModal = (record) => {
    if (record) {
      setModalTaskId(record);
      setVisible(true);
    } else {
      setVisible(false);
    }
  };
  const handleOkModalDelete = () => {
    axios
      .delete(`http://localhost:3222/produk_agen/${modalTaskId.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token_agen")}`,
        },
      })
      .then((res) => {
        console.log(res);
      });
    setModalText("The modal will be closed after one second");
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 1000);
    getData();
    // location.reload()
  };

  const temp = dataProduk.map((values) => {
    const data = {
      nama: values.agen.nama,
      nama_produk: values.nama_produk,
      stok: values.stok,
      deskripsi: values.deskripsi,
      harga: values.harga,
    };
    return data;
  });

  const [nama, setNama] = useState("");
  const [produk, setProduk] = useState("");
  const [stok, setStok] = useState("");
  const [harga, setHarga] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [image, setImage] = useState("");
  const onFinish = async (values) => {
    try {
      const getData = new FormData();
      getData.append("email", values.email);
      getData.append("nama_produk", values.nama_produk);
      getData.append("harga", values.harga);
      getData.append("deskripsi", values.deskripsi);
      getData.append("stok", values.stok);
      getData.append("foto", image);
      // const data = {
      //   email: values.email,
      //   nama_produk: values.nama_produk,
      //   harga: values.harga,
      //   deskripsi: values.deskripsi,
      //   stok: values.stok,
      //   foto: values.image,
      // };
      for (const value of getData.values()) {
        console.log(value);
      }

      const response = await axios
        .post("http://localhost:3222/produk_agen", getData, {
          headers: {
            "Content-type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("token_agen")}`,
          },
        })
        .then((res) => {
          console.log(res);
        });
    } catch (error) {
      if (error) {
        console.log(error);
      }
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    console.log("Clicked cancel button");
    setVisible(false);
    setVisibleDua(false);
    setIsModalVisible(false);
  };

  //upload foto produk
  const props = {
    name: "file",
    action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
    headers: {
      authorization: "authorization-text",
    },

    onChange(info) {
      if (info.file.status !== "uploading") {
      }

      if (info.file.status === "done") {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  const onChangeImage = (e) => {
    // const value = e.target.files[0];

    setImage(e.file.originFileObj);
  };
  return (
    <div className="bg-white rounded-md align-middle my-10 mx-12 pb-5">
      {/* <Layout style={{ backgroundColor: "#A6EEC7" }}> */}
      <Content style={{ marginTop: "10px" }}>
        <Row justify="end">
          <Col span={22} pull={1} className="bg-white">
            <Row justify="end" className="mt-5 mr-5">
              <Col span={3} pull={2}>
                <Button
                  type="primary"
                  onClick={showModal}
                  style={{ marginBottom: "20px", marginLeft: "50px" }}
                >
                  <PlusOutlined />
                  Add Product
                </Button>

                <Modal
                  title="Add Product"
                  visible={isModalVisible}
                  onOk={handleOk}
                  onCancel={handleCancel}
                  footer={false}
                >
                  <Form
                    name="basic"
                    form={form}
                    initialValues={{
                      remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                  >
                    <h1 className="text-start">Email agen</h1>
                    <Form.Item
                      name="email"
                      rules={[
                        {
                          required: true,
                          message: "Please input Name of profuct!",
                        },
                      ]}
                    >
                      <Input placeholder="Email agen" />
                    </Form.Item>
                    <h1 className="text-start">Nama produk</h1>
                    <Form.Item
                      name="nama_produk"
                      rules={[
                        {
                          required: true,
                          message: "Please input Name of profuct!",
                        },
                      ]}
                    >
                      <Input placeholder="Produk" />
                    </Form.Item>
                    <h1 className="text-start">Stok</h1>
                    <Form.Item
                      name="stok"
                      rules={[
                        {
                          required: true,
                          message: "Please input stok!",
                        },
                      ]}
                    >
                      <Input placeholder="stok" />
                    </Form.Item>
                    <h1 className="text-start">Harga</h1>
                    <Form.Item
                      name="harga"
                      rules={[
                        {
                          required: true,
                          message: "Please input Price!",
                        },
                      ]}
                    >
                      <Input placeholder="Harga" />
                    </Form.Item>
                    <h1 className="text-start">Deskripsi</h1>
                    <Form.Item
                      name="deskripsi"
                      rules={[
                        {
                          required: true,
                          message: "Please input your Description of Product!",
                        },
                      ]}
                    >
                      <Input.TextArea placeholder="Deskripsi" />
                    </Form.Item>
                    <Form.Item
                      rules={[
                        {
                          required: true,
                        },
                      ]}
                    >
                      <Upload
                        multiple={false}
                        onChange={onChangeImage}
                        listType="picture"
                      >
                        <Button
                          type="primary"
                          icon={<UploadOutlined />}
                          style={{
                            width: "230px",
                            backgroundColor: "white",
                            border: "black",
                            color: "black",
                          }}
                        >
                          Upload Bukti Bayar
                        </Button>
                      </Upload>
                    </Form.Item>
                    <Form.Item
                      wrapperCol={{
                        offset: 8,
                        span: 8,
                      }}
                    >
                      <Row>
                        <Col span={4} pull={4}>
                          <Button
                            type="primary"
                            htmlType="cancel"
                            onClick={handleCancel}
                          >
                            Cancel
                          </Button>
                        </Col>
                        <Col span={4} offset={12}>
                          <Button
                            type="primary"
                            htmlType="submit"
                            onClick={handleOk}
                          >
                            Submit
                          </Button>
                        </Col>
                      </Row>
                    </Form.Item>
                  </Form>
                </Modal>
              </Col>
            </Row>
          </Col>
        </Row>

        <Row justify="center" align="middle">
          <Col span={22}>
            <Table
              columns={getColumns(updateModal, deleteModal)}
              dataSource={dataProduk}
            />
            <Modal
              name="basic"
              title="Konfirmasi Penghapusan"
              visible={visible}
              onOk={handleOkModalDelete}
              confirmLoading={confirmLoading}
              onCancel={handleCancel}
            >
              <p className="text-emerald-500">
                Apakah anda yakin akan meghapus produk
              </p>
              <p className="text-emerald-500">
                {JSON.stringify(modalTaskId.nama)}
              </p>
            </Modal>
            <Modal
              title="Konfirmasi Update Data"
              visible={visibleDua}
              onOk={handleOkModalUpdate}
              confirmLoading={confirmLoading}
              onCancel={handleCancel}
              // footer={false}
            >
              <Form
                form={form}
                name="basic"
                labelCol={{}}
                wrapperCol={{}}
                initialValues={{
                  remember: true,
                }}
                onFinish={handleOkModalUpdate}
                // onFinishFailed={onFinishFailed}
                // autoComplete="off"
              >
                <h1 className="text-start">Nama Produk</h1>
                <Form.Item name="nama_produk">
                  <Input />
                </Form.Item>

                <h1 className="text-start">Harga</h1>
                <Form.Item name="harga">
                  <Input />
                </Form.Item>
                <h1 className="text-start">deskripsi</h1>
                <Form.Item name="deskripsi">
                  <Input />
                </Form.Item>

                {/* <Form.Item>
                  <Row>
                    <Col span={6} push={5} offset={3}>
                      <Button
                        type="primary"
                        htmlType="cancel"
                        onClick={handleCancel}
                      >
                        Cancel
                      </Button>
                    </Col>
                    <Col span={4} offset={4}>
                      <Button
                        type="primary"
                        htmlType="submit"
                        onClick={handleOkModalUpdate}
                      >
                        Submit
                      </Button>
                    </Col>
                  </Row>
                </Form.Item> */}
              </Form>
            </Modal>
          </Col>
        </Row>
      </Content>
      {/* </Layout> */}
    </div>
  );
}
