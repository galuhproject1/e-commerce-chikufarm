import KontenProduk from "./konten/kontenProduct";
import MainLayout from "./Layout/MainLayout";

function ProductAdmin() {
  return (
    <>
      <MainLayout>
        <KontenProduk />
      </MainLayout>
    </>
  );
}

export default ProductAdmin;
