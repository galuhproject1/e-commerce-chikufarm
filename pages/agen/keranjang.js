import Kontenkeranjang from "./konten/kontenKeranjang";
import MainLayout from "./Layout/MainLayout";

function keranjangAgen() {
  return (
    <>
      <MainLayout>
        <Kontenkeranjang />
      </MainLayout>
    </>
  );
}

export default keranjangAgen;
