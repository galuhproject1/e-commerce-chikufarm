import KontenCustomer from "./konten/kontenCustomer";
import MainLayout from "./Layout/MainLayout";

function AgenCustomer() {
  return (
    <>
      <MainLayout>
        <KontenCustomer />
      </MainLayout>
    </>
  );
}

export default AgenCustomer;
