import "antd/dist/antd.css";
import "tailwindcss/tailwind.css";
import {
  Col,
  Row,
  Grid,
  Card,
  Table,
  Button,
  Space,
  Input,
  Tabs,
  Tooltip,
  message,
  Modal,
} from "antd";
import { Content } from "antd/lib/layout/layout";
import React, { useState, useEffect } from "react";
import Product1 from "../image/ayam2 2.png";
import Product2 from "../image/telur-ayam 1.png";
import axios from "axios";
import { useRouter } from "next/router";
import jwt_decode from "jwt-decode";
import Link from "next/link";

function getColumns(deleteModal) {
  return [
    {
      title: "Nama produk",
      dataIndex: "produkAgen",
      key: "produkAgen",
      render: (_, record) => <p>{record.produkAgen.nama_produk}</p>,
    },
    {
      title: "Harga Produk",
      dataIndex: "harga",
      key: "harga",
      render: (_, record) => <p>{record.produkAgen.harga}</p>,
    },
    {
      title: "Jumlah Produk",
      dataIndex: "jumlah_produk",
      key: "jumlah_produk",
    },
    {
      title: "Total",
      key: "total_harga",
      dataIndex: "total_harga",
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Tooltip placement="right" title="Delete">
            <Button
              type="danger"
              onClick={() => deleteModal(record)}
              danger={true}
            >
              Remove
            </Button>
          </Tooltip>
        </Space>
      ),
    },
  ];
}

export default function OnGoing() {
  const [loading, setLoading] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [loadingDua, setLoadingDua] = useState(false);
  // state modal delete
  const [visible, setVisible] = useState(false);
  const [modalText, setModalText] = useState("Content of the modal");
  const [modalTaskId, setModalTaskId] = useState("");

  const handleCancel = () => {
    console.log("Clicked cancel button");
    setVisible(false);

    // setIsModalVisible(false);
  };

  //total bayar
  // const [totalBayar, setTotalBayar]

  // modal delete
  const deleteModal = (record) => {
    if (record) {
      setModalTaskId(record);
      setVisible(true);
    } else {
      setVisible(false);
    }
  };
  const handleOkModalDelete = () => {
    axios
      .delete(`http://localhost:3222/cart/${modalTaskId.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token_customer")}`,
        },
      })

      .then((res) => {
        console.log(res, "berhasil");
      })
      .then(getData())
      .then(setIsCart(false));
    setModalText("The modal will be closed after one second");
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 1000);
  };

  const [dataCart, setDataCart] = useState([]);
  const [total_bayar, setTotal_Bayar] = useState([]);
  async function getData() {
    try {
      await axios
        .get("http://localhost:3222/cart", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_customer")}`,
          },
        })
        .then((res) => {
          // console.log(res.data.data);
          setDataCart(res.data.data);
          setTotal_Bayar(res.data.data[0].total_harga);
          setIsCart(true);
        });
    } catch (error) {
      // message.error("error");
      console.log(error.message, "dasdfd");
      // if (error) {
      //   //
      // }
    }
  }

  const [isCart, setIsCart] = useState(false);
  // const getCart = () => {
  //   if (!dataCart) {
  //     setIsCart(true);
  //   } else {
  //     setIsCart(false);
  //   }
  // };
  // useEffect(() => {
  //   getCart();
  // }, []);

  // const [total_bayar, setTotal_Bayar] = useState([]);
  // const totalBayar = async () => {
  //   const hasil = await dataCart.reduce((value, i) => {
  //     return value + i.total_harga;
  //   }, 0);
  //   setTotal_Bayar(hasil);
  // };
  // totalBayar();

  useEffect(() => {
    getData();
    // getCart();
  }, []);

  return (
    <div className="mb-10">
      <div className="min-h-screen pt-1">
        <Row justify="center">
          <Col
            lg={{ span: 20 }}
            md={{ span: 22 }}
            sm={{ span: 22 }}
            xs={{ span: 24 }}
            style={{ height: "auto" }}
          >
            <Table
              columns={getColumns(deleteModal)}
              dataSource={dataCart}
              size="large"
              pagination={false}
            />
            <Modal
              name="basic"
              title="Konfirmasi Penghapusan"
              visible={visible}
              onOk={handleOkModalDelete}
              confirmLoading={confirmLoading}
              onCancel={handleCancel}
            >
              <p className="text-emerald-500">
                Apakah anda yakin akan meghapus produk
              </p>
              <p className="text-emerald-500">
                {JSON.stringify(modalTaskId.nama_produk)}
              </p>
            </Modal>
          </Col>
        </Row>
        {isCart ? (
          <Row className="flex justify-end mr-40 mt-4 ">
            <Col>
              <h3>Sub-total</h3>
            </Col>
            <Col className="px-10" data key={dataCart}>
              <h3>
                {/* {total_bayar} */}
                {/* total */}
                {total_bayar}
              </h3>
            </Col>

            <Col className=" ">
              <Button
                type="primary"
                style={{
                  backgroundColor: "#56B280",
                  border: "none",
                  hover: "none",
                }}
                // onClick={() => {
                //   setDataCart(null);
                // }}
              >
                <Link href="/payment">Checkout</Link>
              </Button>
            </Col>
          </Row>
        ) : (
          <p> </p>
        )}
      </div>
    </div>
  );
}
