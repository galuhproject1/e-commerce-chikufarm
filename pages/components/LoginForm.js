import { Button, Checkbox, Form, Input, message } from "antd";
import React from "react";
import "antd/dist/antd.css";
import Link from "next/link";
import axios from "axios";
import { useState } from "react";
import Router, { useRouter } from "next/router";
import jwt_decode from "jwt-decode";

const LoginForm = () => {
  const [form] = Form.useForm();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [token, setToken] = useState("");

  const onFinish = async (values) => {
    try {
      const formData = {
        email: values.email,
        password: values.password,
      };
      console.log("Success:", formData);
      const request = await axios
        .post("http://localhost:3222/auth", formData, {
          headers: { "content-type": "application/json" },
        })
        .then((result) => {
          console.log(result, "res api");

          const decode = jwt_decode(result.data.token);

          // console.log(decode, "isi decode");

          if (decode.rolename == "admin") {
            localStorage.setItem("token_admin", result.data.token);
            router.push("/admin/dashboard");
          } else if (decode.rolename == "agen") {
            localStorage.setItem("token_agen", result.data.token);
            router.push("/agen/dashboard");
          } else if (decode.rolename == "customer") {
            localStorage.setItem("token_customer", result.data.token);
            router.push(`/landing`);
          }
        });
    } catch (error) {
      window.alert("username atau password salah");
      console.error(error);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const onChangeUsername = (e) => {
    const value = e.target.value;
    setUsername(value);
  };

  const onChangePassword = (e) => {
    const value = e.target.value;
    setPassword(value);
  };
  const onFormSubmit = (e) => {
    e.preventDefault();
  };

  const router = useRouter();

  return (
    <div>
      <Form
        form={form}
        name="basic"
        wrapperCol={{
          span: 24,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input
            style={{
              width: "100%",
              borderRadius: 10,
            }}
            placeholder="username"
          />
        </Form.Item>

        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password
            style={{
              width: "100%",
              borderRadius: 10,
            }}
            placeholder="password"
          />
        </Form.Item>

        <Form.Item>
          <div>
            <Button
              htmlType="submit"
              style={{
                backgroundColor: "#76323F",
                width: 130,
                height: 30,
                borderRadius: 10,
                borderColor: "#76323f",
                marginRight: 10,
                color: "#eee",
                fontWeight: "bold",
              }}
            >
              LOGIN
            </Button>
            <Button
              htmlType="Signup"
              style={{
                backgroundColor: "#F9EAE1",
                width: 130,
                height: 30,
                borderRadius: 10,
                marginLeft: 10,
                fontWeight: "bold",
              }}
            >
              <Link href="./register/register">SIGN UP</Link>
            </Button>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
};

export default LoginForm;
