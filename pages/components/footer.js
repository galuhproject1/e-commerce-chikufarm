import "antd/dist/antd.css";
import "tailwindcss/tailwind.css";
import Image from "next/image";
import Logo from "../image/logo.png";

function Footer() {
  return (
    <div>
      <footer className="text-start lg:text-left bg-emerald-500 text-white">
        <div className="flex justify-center items-center lg:justify-between p-6 border-b border-[#56B280]-300">
          <div className="flex justify-center pl-10"></div>
        </div>
        <div className="mx-6 py-5 text-center md:text-left">
          <div className="grid grid-1 md:grid-cols-2 lg:grid-cols-4 gap-10">
            <div>
              <Image
                src={Logo}
                objectFit="contain"
                width={241}
                height={80}
                // className="bg-[#E5E5E5] rounded-lg"
              />
              <p className="px-6">
                Farms with processes and quality that are maintained and
                reliable
              </p>
            </div>
            <div className="mx-auto">
              <h6 className="uppercase font-semibold mb-4 flex text-end md:justify-start py-5">
                Discovery
              </h6>
              <p className="mb-4">
                <a href="#!" className="text-white">
                  Product
                </a>
              </p>
            </div>
            <div className="mx-auto">
              <h6 className="uppercase font-semibold mb-4 flex justify-center md:justify-start py-5">
                About
              </h6>
              <p className="mb-4">
                <a href="#!" className="text-white">
                  Help
                </a>
              </p>
              <p className="mb-4">
                <a href="#!" className="text-white">
                  Agen
                </a>
              </p>
            </div>
            <div className="mx-auto">
              <h6 className="uppercase font-semibold mb-4 flex justify-center md:justify-start py-5">
                Info
              </h6>
              <p className="mb-4">
                <a href="#!" className="text-white">
                  Contact us
                </a>
              </p>
              <p className="mb-4">
                <a href="#!" className="text-white">
                  Privacy Policies
                </a>
              </p>
              <p className="mb-4">
                <a href="#!" className="text-white">
                  Term and Condition
                </a>
              </p>
            </div>
          </div>
        </div>
        <div className="text-center p-3 align-middle bg-[#E5E5E5]">
          <p className="text-black">© 2022 Chikufarm. All right reserved</p>
        </div>
      </footer>
    </div>
  );
}

export default Footer;
