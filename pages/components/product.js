import { Tabs, Card } from "antd";
import {
  Col,
  Row,
  Grid,
  Form,
  Select,
  Button,
  Dropdown,
  Menu,
  message,
  Space,
  Tooltip,
} from "antd";
import { DownOutlined, UserOutlined } from "@ant-design/icons";
import { Content } from "antd/lib/layout/layout";
import cardImg1 from "../image/telur-ayam 1.png";
import cardImg2 from "../image/bibit-ayam 1.png";
import cardImg3 from "../image/kandang-ayam 1.png";
import cardImg4 from "../image/ayam2 2.png";
import Image from "next/image";
import { useState, useEffect } from "react";
import "tailwindcss/tailwind.css";
import "antd/dist/antd.css";
import Link from "next/link";
import axios from "axios";

const { Meta } = Card;
const { useBreakpoint } = Grid;
const { TabPane } = Tabs;
const styleTab = {
  fontWeight: "bold",
  fontSize: "50pt",
  borderRight: "solid red 5px",
};

const handleMenuClick = (e) => {
  message.info("Click on menu item.");
  console.log("click", e);
};
const pilihAgen = (
  <Menu
    onClick={handleMenuClick}
    items={[
      {
        label: "Cikarang",
        key: "1",
        icon: <UserOutlined />,
      },
      {
        label: "Pondok Gede",
        key: "2",
        icon: <UserOutlined />,
      },
      {
        label: "Cibitung",
        key: "3",
        icon: <UserOutlined />,
      },
    ]}
  />
);

function ProductHome() {
  const [dataProduct, setDataProduct] = useState([]);
  async function getDataProduct() {
    try {
      // const getToken = localStorage.getItem("token_customer");
      // const decode = jwt_decode(getToken);
      // console.log(getToken)
      await axios
        .get("http://localhost:3222/produk_agen", {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then((res) => {
          // console.log(res.data);
          setDataProduct(res.data.data);
        });
    } catch (error) {
      console.error(error);
    }
  }
  useEffect(() => {
    getDataProduct();
  }, []);

  // console.log(dataProduct);
  const screens = useBreakpoint();
  return (
    <>
      <div className="text-start mt-5 py-5">
        <h4 className=" text-black text-2xl text-center lg:ml-20 lg:text-left md:ml-10 md:text-left sm:ml-10 sm:text-left">
          Products
        </h4>
        <div>
          {Object.entries(screens)
            .filter((screen) => !!screen[1])
            .map((screen) => console.log(screen[0]))}

          <Row>
            <Col lg={{ span: 8 }} offset={1}>
              <p className="text-l ml-7">
                Order it for you or for your beloved ones
              </p>
            </Col>
            <Col lg={{ span: 5 }} push={4} className="lg:text-right ">
              <Dropdown overlay={pilihAgen}>
                <Button>
                  <Space>
                    Pilih Agen
                    <DownOutlined />
                  </Space>
                </Button>
              </Dropdown>
            </Col>
          </Row>
        </div>
      </div>
      <div className="h-full bg-[#fff] pb-10">
        <div className="tab-content" id="tabs-tabContent">
          <div
            className="tab-pane fade show active"
            id="tabs-jakarta"
            role="tabpanel"
            aria-labelledby="tabs-jakarta-tab"
          >
            {/* card product */}

            <Row justify="center space-x-5" className="bg-[#fff]">
              {dataProduct.map((data) => {
                // console.log(data.foto, "data uy");
                return (
                  <>
                    <Col
                      lg={{ span: 5 }}
                      md={{ span: 5 }}
                      sm={{ span: 10 }}
                      xs={{ span: 10 }}
                      className="pt-5"
                    >
                      <Link href={`/produk/${data.id}`}>
                        <Card
                          hoverable
                          className="shadow-lg w-11/12"
                          cover={
                            <Image
                              alt="example"
                              src={`http://localhost:3222/produk_agen/${data.foto}`}
                              width={300}
                              height={200}
                              // priority={"true"}
                            />
                          }
                          data
                          key={data.id}
                        >
                          <Meta
                            title={data.nama_produk}
                            description={`Harga : Rp ${data.harga}`}
                          />
                          <p> Stok : {data.stok}</p>
                        </Card>
                      </Link>
                    </Col>
                  </>
                );
              })}
            </Row>
          </div>
        </div>
      </div>
    </>
  );
}

export default ProductHome;
