import "tailwindcss/tailwind.css";
import "antd/dist/antd.css";
import { Button } from "antd";
import React from "react";
import Image from "next/image";
import GambarKanan from "../image/telur-bebek 2.png";
import Gambar1 from "../image/telur-ayam.jpg";
import Link from "next/link";

function MoreProduct() {
  return (
    <div className="bg-[#A6EEC7] flex justify-center">
      <div className="grid md:grid-cols-2 sm:grid-cols-1 sm:mx-auto md:mx-auto w-10/12 mb-10">
        <div className="w-10/12 mr-10 sm:text-start  sm:ml-auto mt-20">
          <br />
          <h1 className="text-justify text-2xl ">More chicken and duck eggs</h1>
          <p className="text-align-center">
            <i className="fa-regular fa-circle-check"></i> High-Quality: Good
            process and high yield
          </p>
          <p className="text-align-justify">
            <i className="fa-regular fa-circle-check"></i>
            Stable and affordable price
          </p>
          <p className="text-align-justify">
            <i className="fa-regular fa-circle-check"></i>
            Fast and reliable service process
          </p>
          <br />
          <button
            className="bg-[#56B280] border-[#56B280]  shadow hover:translate-x-2 
          hover:transition ease-in-out translate-y-1 active:translate-y-2 text-white rounded-md"
          >
            <Link href="/produk/detail">
              <p className="text-lg mt-1.5 mx-3 mb-1.5">Learn More</p>
            </Link>
          </button>
        </div>
        <Image
          src={Gambar1}
          width={293}
          height={555}
          objectFit="contain"
          className="w-10/12 ml-5"
        />
      </div>
    </div>
  );
}

export default MoreProduct;
