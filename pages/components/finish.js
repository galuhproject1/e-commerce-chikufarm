import "antd/dist/antd.css";
import "tailwindcss/tailwind.css";
import { Col, Row, Grid, Card, Table, Button, Space, Input, Tabs } from "antd";
import { Content } from "antd/lib/layout/layout";
import React, { useState } from "react";
import Product1 from "../image/telur-ayam 1.png";

import Link from "next/link";
import Image from "next/image";

const columns = [
  {
    title: "Product",
    dataIndex: "product",
    key: "product",
    width: "100px",
  },
  {
    title: "Resume",
    dataIndex: "resume",
    key: "resume",
    width: "400px",
  },
];
const data = [
  {
    key: "1",
    product: (
      <div className="justify-start ">
        <Row>
          <Col span={24}>
            <Image src={Product1} className="" />
          </Col>
        </Row>
      </div>
    ),
    resume: (
      <div>
        <Row>
          <Col span={24}>
            <Card>
              <p>INV/2022/07/11/XVI/0987654321</p>
              <p>Produk: Telur Ayam</p>
              <p>Jumlah: 1Kg</p>
              <p>Tanggal Pembelian: 11/07/2022 </p>
              <p>Total : 25.000</p>
            </Card>
          </Col>
        </Row>
      </div>
    ),
  },
];

export default function Finish() {
  return (
    <div className="min-h-screen pt-1">
      <Row justify="center">
        <Col
          lg={{ span: 20 }}
          md={{ span: 22 }}
          sm={{ span: 22 }}
          xs={{ span: 24 }}
        >
          <Table
            columns={columns}
            dataSource={data}
            size="large"
            pagination={false}
            showHeader={false}
          />
        </Col>
      </Row>
    </div>
  );
}
