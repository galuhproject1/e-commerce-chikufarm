/* This example requires Tailwind CSS v2.0+ */
import "tailwindcss/tailwind.css";
import image1 from "../image/logo.png";
import Link from "next/link";

import "tailwindcss/tailwind.css";
import { useState } from "react";

import Image from "next/image";

export default function NavigasiPayment() {
  const [navbar, setNavbar] = useState(false);
  return (
    <>
      <div
        className="w-full bg-white shadow fixed-top"
        style={{ position: "fixed", zIndex: "999" }}
      >
        <div className="justify-between px-4 mx-auto  md:items-center md:flex md:px-8">
          <div className="flex items-center justify-between md:block">
            <Link href="/landing">
              <a>
                <Image
                  src={image1}
                  width={142}
                  height={50}
                  objectFit="contain"
                />
              </a>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}
