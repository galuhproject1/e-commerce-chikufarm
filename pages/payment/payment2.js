import NavigasiPayment from "../components/NavbarPayment";
import {
  Row,
  Col,
  Card,
  Form,
  Button,
  Input,
  Upload,
  message,
  Select,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import "antd/dist/antd.css";
import Image from "next/image";
import Product1 from "../image/telur-ayam 1.png";
import { useState, useEffect } from "react";
import jwtDecode from "jwt-decode";
import axios from "axios";
import Router, { useRouter } from "next/router";

const props = {
  name: "file",
  action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
  headers: {
    authorization: "authorization-text",
  },

  onChange(info) {
    if (info.file.status !== "uploading") {
      console.log(info.file, info.fileList);
    }

    if (info.file.status === "done") {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};

export default function PaymnetDua() {
  const [name, setName] = useState("");

  const [nama_produk, setNama_Produk] = useState([]);
  const [nama_pembeli, setNama_Pembeli] = useState([]);
  const [id_cart, setId_Cart] = useState([]);
  const [image, setImage] = useState("");
  const [formPayment] = Form.useForm();
  const router = useRouter();
  // get data for form
  const [dataCart, setDataCart] = useState([]);
  async function getDataCart() {
    try {
      // const getToken = localStorage.getItem("token_customer");
      // const decode = jwtDecode(getToken);
      // const Nama = decode.name;
      // setName(Nama);
      // console.log(decode.name);
      await axios
        .get("http://localhost:3222/cart", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_customer")}`,
          },
        })
        .then((res) => {
          // console.log(res.data.data[0].pembeli.email, "ini data cart");
          setDataCart(res.data.data);
          setNama_Produk(res.data.data[0].ProdukAgen.nama_produk);
          setName(res.data.data[0].pembeli.email);
          // console.log(res.data.data[0], "produk cuy");
        });
    } catch (error) {
      if (error) {
        // message.error("error");
      }
    }
  }
  const [total_bayar, setTotal_Bayar] = useState([]);

  const [no_rekening, setNo_Rekening] = useState([]);
  async function getDataBank() {
    try {
      await axios
        .get("http://localhost:3222/bank", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_customer")}`,
          },
        })
        .then((res) => {
          // console.log(res.data.data[0]?.no_rekening, "ini data bank");
          setDataBank(res.data.data[0]?.no_rekening);
        });
    } catch (error) {
      if (error) {
        // message.error("error");
      }
    }
  }

  useEffect(() => {
    getDataCart();
    getDataBank();
  }, []);

  formPayment.setFieldsValue({
    nama_pembeli: nama_pembeli,
    nama_produk: nama_produk,
    no_rekening: no_rekening,
    id_cart: dataCart,
    name: name,
    email: name,
  });

  const onChangeNama_Pembeli = (e) => {
    const value = e.target.value;
    setNama_Pembeli(value);
  };
  const onChangeNama_Produk = (e) => {
    const value = e.target.value;
    setNama_Produk(value);
  };
  const onChangeTotal_Bayar = (e) => {
    const value = e.target.value;
    setTotal_Bayar(value);
  };

  const onChangeId_Cart = (e) => {
    const value = e.target.value;
    setId_Cart(value);
  };

  const onFormSubmit = (e) => {
    e.preventDefault();
    console.log("ok");
  };

  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const onChangeImage = (e) => {
    // const value = e.target.files[0];
    console.log(e);
    setImage(e.file.originFileObj);
  };

  const { Option } = Select;

  const handleChange = (value) => {
    console.log(`selected ${value}`);
    setNo_Rekening(value);
  };

  function postData() {
    const data = new FormData();

    dataCart.map((e) => {
      console.log(e);
      data.append("total_bayar", e.total_harga);
      data.append("nama_pembeli", e.pembeli.nama);
      data.append("nama_produk", e.produkAgen.nama_produk);
      data.append("id_cart", e.id);
      data.append("email", e.pembeli.email);
    });
    data.append("no_rekening", no_rekening);
    data.append("bukti_bayar", image);

    for (const value of data.values()) {
      console.log(value);
    }

    axios
      .post("http://localhost:3222/transaksi_pembeli/create", data, {
        headers: {
          "Content-type": "multipart/form-data",
          Authorization: `Bearer ${localStorage.getItem("token_customer")}`,
        },
      })
      .then((res) => {
        console.log(res);
        if (res.status == 200 || res.status == 201) {
          message.info("Selamat Transaksi anda sedang di proses");
          setTimeout(() => {
            router.push("/payment/paymentConfirm");
          }, 3000);
        }
      });
  }
  return (
    <>
      <NavigasiPayment />
      <Row>
        <Col span={12}>
          <div className="flex flex-col pt-20 mx-20 h-screen">
            <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
                {/* <div className="overflow-hidden ">
                  <table className="min-w-full">
                    <tbody>
                      <tr className="bg-white border-b">
                        <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                          Contact
                        </td>
                        <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                          081249781649
                        </td>
                        <td className="text-sm text-emerald-600 font-light px-6 py-4 whitespace-nowrap">
                          <button>Edit</button>
                        </td>
                      </tr>
                      <tr className="bg-white border-b">
                        <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                          Ship to
                        </td>
                        <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                          jatibening, Bekasi
                        </td>
                        <td className="text-sm text-emerald-600 font-light px-6 py-4 whitespace-nowrap">
                          <button>Edit</button>
                        </td>
                      </tr>
                      <tr className="bg-white border-b">
                        <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                          Method
                        </td>
                        <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                          Standard shiping - FREE
                        </td>
                        <td className="text-sm text-emerald-600 font-light px-6 py-4 whitespace-nowrap">
                          <button>Edit</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div> */}
              </div>
              <div className="mx-8 mt-5 mb-3">
                <h1>Paymnet Method</h1>
              </div>
              <div className="flex justify-center">
                <Col span={22} className="pl-3">
                  <Card
                    style={{
                      width: 480,
                      height: 58,
                      backgroundColor: "#A6EEC7",
                    }}
                  >
                    <Row>
                      <Col className="text-2xl -mt-3">
                        <i className="fa-solid fa-credit-card text-emerald-900"></i>
                      </Col>
                      <Col>
                        <p className="text-md -mt-2 mx-5 font-bold">Transfer</p>
                      </Col>
                    </Row>
                  </Card>
                  <Card
                    style={{
                      width: 480,
                      height: "auto",
                    }}
                  >
                    {dataCart.map((data) => {
                      return (
                        <>
                          <ul key={data.id}>
                            <li className="invisible -mt-5">
                              {data.pembeli?.email}
                            </li>
                            <li className="invisible -mt-5">{data.id}</li>
                            <li className="invisible -mt-5">
                              {data.jumlah_produk}
                            </li>
                            <li className="invisible -mt-5">
                              {data.total_harga}
                            </li>
                            <li className="invisible -mt-5">
                              {data.produkAgen?.nama_produk}
                            </li>
                            <li className="invisible">{data.bank}</li>
                          </ul>
                          <Form.Item>
                            <Select
                              defaultValue="Agen Cibitung"
                              style={{
                                width: 430,
                              }}
                              onChange={handleChange}
                            >
                              <Option value="6500 070 1908 2022">
                                Agen Jatibening - BRI - 6500 070 1908 2022
                              </Option>
                              <Option value="Agen Cikarang">
                                Agen Cikarang - BRI - 6500 70xx xxx xxxx
                              </Option>
                              <Option value="Agen Jatibening">
                                Agen Jatibening - BRI - 6500 70xx xxx xxxx
                              </Option>
                            </Select>
                          </Form.Item>
                          <Form.Item
                            rules={[
                              {
                                required: true,
                              },
                            ]}
                          >
                            <Upload
                              multiple={false}
                              onChange={onChangeImage}
                              listType="picture"
                            >
                              <Button
                                type="primary"
                                icon={<UploadOutlined />}
                                style={{
                                  width: "430px",
                                  backgroundColor: "#A6EEC7",
                                  border: "none",
                                  color: "black",
                                }}
                              >
                                Upload Bukti Bayar
                              </Button>
                            </Upload>
                          </Form.Item>
                        </>
                      );
                    })}
                  </Card>
                </Col>
              </div>
            </div>

            <div className="absolute space-x-60 justify-center bottom-5">
              <button
                type="link"
                className="inline-block  text-emerald-500 underline font-small text-sm leading-tight transition duration-150 ease-in-out"
              >
                <a href="/payment" className="text-emerald-500">
                  Cancel
                </a>
              </button>
              <button
                onClick={() => postData()}
                type="button"
                className="inline-block px-8 py-2 bg-[#56B280] text-white text-md leading-tight shadow-md hover:bg-emerald-700 hover:shadow-lg focus:bg-emerald-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-emerald-800 active:shadow-lg transition duration-150 ease-in-out"
              >
                Go to payment
              </button>
            </div>
          </div>
        </Col>
        <Col span={12}>
          <Card style={{ backgroundColor: "#F2F2F2", height: "100%" }}>
            <Row className="border-b border-[#aaa]-300 mx-5 my-10"></Row>
            <Row className="mx-5">
              <Col span={6}>
                <p>Subtotal</p>
              </Col>
              <Col span={6} offset={12}>
                <h1>{dataCart[0]?.produkAgen?.nama_produk}</h1>
              </Col>
            </Row>
            <Row className="mx-5">
              <Col span={6}>
                <p>Shiping</p>
              </Col>
              <Col span={6} offset={12}>
                <p>free shiping</p>
              </Col>
            </Row>
            <Row className="border-b border-[#aaa]-300 mx-5 my-10"></Row>
            <Row className="mx-5">
              <Col span={6}>
                <p>Total</p>
              </Col>
              <Col span={6} offset={12}>
                <h1 className="text-xl">{dataCart[0]?.total_harga}</h1>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </>
  );
}
