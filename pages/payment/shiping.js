import NavigasiPayment from "../components/NavbarPayment";
import { Row, Col, Card } from "antd";
import "antd/dist/antd.css";

import Image from "next/image";
import Product1 from "../image/telur-ayam 1.png";

export default function Shiping() {
  return (
    <>
      <NavigasiPayment />
      <Row>
        <Col span={12}>
          <div className="flex flex-col pt-20 mx-20 h-screen">
            <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
                <div className="overflow-hidden ">
                  <table className="min-w-full">
                    <tbody>
                      <tr className="bg-white border-b">
                        <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                          Contact
                        </td>
                        <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                          081249781649
                        </td>
                        <td className="text-sm text-emerald-600 font-light px-6 py-4 whitespace-nowrap">
                          <button>Edit</button>
                        </td>
                      </tr>
                      <tr className="bg-white border-b">
                        <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                          Ship to
                        </td>
                        <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                          jatibening, Bekasi
                        </td>
                        <td className="text-sm text-emerald-600 font-light px-6 py-4 whitespace-nowrap">
                          <button>Edit</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div>
              <h1 className=" text-xl my-5">Shiping Method</h1>
            </div>
            <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
                <div className="overflow-hidden ">
                  <table className="min-w-full">
                    <tbody>
                      <tr className="bg-white border-b">
                        <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                          <div className="form-check">
                            <input
                              className="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-lg bg-white checked:bg-emerald-600 checked:border-emerald-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                              type="checkbox"
                              defaultValue=""
                              id="flexCheckDefault"
                            />
                            <label
                              className="form-check-label inline-block text-gray-800"
                              htmlFor="flexCheckDefault"
                            >
                              Standard Shipping
                            </label>
                          </div>
                        </td>

                        <td className="text-mdS font-bold px-6 py-4 whitespace-nowrap">
                          Free
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div className="absolute space-x-60 justify-center bottom-5">
              <button
                type="link"
                className="inline-block  text-emerald-500 underline font-small text-sm leading-tight transition duration-150 ease-in-out"
              >
                <a href="/payment" className="text-emerald-500">
                  Back to detail
                </a>
              </button>
              <button
                type="button"
                className="inline-block px-8 py-2 bg-[#56B280] text-white text-md leading-tight shadow-md hover:bg-emerald-700 hover:shadow-lg focus:bg-emerald-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-emerald-800 active:shadow-lg transition duration-150 ease-in-out"
              >
                <a href="/payment/payment2" className="text-white">
                  Go to payment
                </a>
              </button>
            </div>
          </div>
        </Col>
        <Col span={12}>
          <Card style={{ backgroundColor: "#F2F2F2", height: "100%" }}>
            <Row className="border-b border-[#aaa]-300 mx-5 my-10"></Row>
            <Row className="mx-5">
              <Col span={6}>
                <p>Subtotal</p>
              </Col>
              <Col span={6} offset={12}>
                <h1>Rp. 25.000</h1>
              </Col>
            </Row>
            <Row className="mx-5">
              <Col span={6}>
                <p>Shiping</p>
              </Col>
              <Col span={6} offset={12}>
                <p>free shiping</p>
              </Col>
            </Row>
            <Row className="border-b border-[#aaa]-300 mx-5 my-10"></Row>
            <Row className="mx-5">
              <Col span={6}>
                <p>Total</p>
              </Col>
              <Col span={6} offset={12}>
                <h1 className="text-xl">Rp. 25.000</h1>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </>
  );
}
