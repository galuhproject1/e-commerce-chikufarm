import NavigasiPayment from "../components/NavbarPayment";
import { Button, Card, Checkbox, Col, Form, Input, Row, message } from "antd";
import "antd/dist/antd.css";
import { useState, useEffect } from "react";
import Link from "next/link";
import React from "react";
import Product1 from "../image/telur-ayam 1.png";
import Image from "next/image";
import axios from "axios";
import jwtDecode from "jwt-decode";

function payment() {
  const [dataUser, setDatauser] = useState([]);
  const [nama, setNama] = useState([]);
  const [alamat, setAlamat] = useState([]);
  const [no_hp, setNo_Hp] = useState([]);
  // const [tester, setTester] = useState([]);

  const [formUser] = Form.useForm();

  const [dataCart, setDataCart] = useState([]);
  async function getDataCart() {
    try {
      await axios
        .get("http://localhost:3222/cart", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_customer")}`,
          },
        })
        .then((res) => {
          console.log(res.data.data);
          setDataCart(res.data.data);
        });
    } catch (error) {
      if (error) {
        message.error("error");
      }
    }
  }
  console.log(dataCart[0]?.produkAgen?.nama_produk);
  const getTokenJwt = async () => {
    try {
      const getToken = localStorage.getItem("token_customer");
      const decode = await jwtDecode(getToken);
      const response = await axios.get(
        `http://localhost:3222/users/${decode.id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_customer")}`,
          },
        }
      );
      console.log(response.data.data, "ini dari response");
      setNama(response.data.data.nama);
      setAlamat(response.data.data.alamat);
      setNo_Hp(response.data.data.no_hp);
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    getTokenJwt();
    getDataCart();
  }, []);

  formUser.setFieldsValue({
    nama: nama,
    alamat: alamat,
    no_hp: no_hp,
  });

  // useEffect(() => {
  //   getData();
  //   // getDataUser();
  //   form.getFieldValue({
  //     // username: { tester },
  //     nama: setNama(),
  //   });
  // }, []);

  const handleFinish = (value) => {
    console.log(value);
  };

  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <>
      <NavigasiPayment />
      <Row>
        <Col span={12}>
          <Card className="h-screen">
            <Row className="mt-20 justify justify-center">
              {/* <Form
                form={formUser}
                name="nama"
                wrapperCol={{
                  span: 24,
                }}
                initialValues={{
                  remember: true,
                }}
              >
                <h1 className="text-xl">Contact</h1>
                <Form.Item
                  name="No_Hp"
                  style={{ width: "450px" }}
                  rules={[
                    {
                      required: true,
                      message: "Please input your phone or email!",
                    },
                  ]}
                >
                  <Input placeholder="email or mobile phone number" />
                </Form.Item>
              </Form> */}
              {/* <h1>Atas Nama : {tester}</h1> */}
            </Row>
            <Row className="justify-center">
              <Form
                form={formUser}
                name="basic"
                wrapperCol={{
                  span: 24,
                }}
                initialValues={{
                  remember: true,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
              >
                <h1 className="text-l ml-20">Shipping Address</h1>
                <Row gutter={45} className="justify-center">
                  <Col>
                    <Form.Item name="nama" style={{ width: "445px" }}>
                      <Input placeholder="Fullname" />
                    </Form.Item>
                  </Col>

                  <Form.Item name="alamat" style={{ width: "445px" }}>
                    <Input placeholder="alamat lengkap" />
                  </Form.Item>
                  <Form.Item name="no_hp" style={{ width: "445px" }}>
                    <Input placeholder="Nomor telepon" />
                  </Form.Item>
                </Row>
                <Row className="-mt-5">
                  <Col span={4} push={2}>
                    <Button
                      type="link"
                      block
                      href="/customer/pengaturan"
                      style={{
                        color: "green",
                      }}
                    >
                      edit
                    </Button>
                  </Col>
                </Row>
                <Row className="mt-10">
                  <Col span={4} push={2}>
                    <Button
                      type="link"
                      block
                      href="/cart/keranjang"
                      style={{
                        color: "green",
                      }}
                    >
                      Back to Cart
                    </Button>
                  </Col>
                  <Col span={4} offset={10} push={1}>
                    <Button
                      type="primary"
                      style={{
                        backgroundColor: "#56B280",
                        paddingLeft: "30px",
                        paddingRight: "30px",
                        borderColor: "#56B280",
                      }}
                      href="/payment/payment2"
                    >
                      Go to Shiping
                    </Button>
                  </Col>
                </Row>
              </Form>
            </Row>
          </Card>
        </Col>
        <Col span={12}>
          <Card style={{ backgroundColor: "#F2F2F2", height: "100%" }}>
            <Row className="mx-5 mt-10 pt-10">
              <Col span={6}>
                <p>Produk</p>
              </Col>
              <Col span={6} offset={12} data key={dataCart}>
                <h1>{dataCart[0]?.produkAgen?.nama_produk}</h1>
              </Col>
            </Row>
            <Row className="mx-5">
              <Col span={6}>
                <p>Subtotal</p>
              </Col>
              <Col span={6} offset={12}>
                <h1>{dataCart[0]?.total_harga}</h1>
              </Col>
            </Row>
            <Row className="mx-5">
              <Col span={6}>
                <p>Shiping</p>
              </Col>
              <Col span={4} offset={12}>
                <p>Free Shiping</p>
              </Col>
            </Row>
            <Row className="border-b border-[#aaa]-300 mx-5 my-10"></Row>
            <Row className="mx-5">
              <Col span={6}>
                <p>Total</p>
              </Col>
              <Col span={6} offset={11}>
                <h1 className="text-xl">{dataCart[0]?.total_harga}</h1>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </>
  );
}

export default payment;
