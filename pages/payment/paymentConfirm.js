import NavigasiPayment from "../components/NavbarPayment";
import { Row, Col, Card, Form, Button, Input, Upload, message } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import axios from "axios";
import { useEffect, useState } from "react";
import jwtDecode from "jwt-decode";
import "antd/dist/antd.css";

export default function PaymnetConfirm() {
  // const [loading, setLoading] = useState(false);
  // const [confirmLoading, setConfirmLoading] = useState(false);
  // const [loadingDua, setLoadingDua] = useState(false);
  // // state modal delete
  // const [visible, setVisible] = useState(false);
  // const [modalText, setModalText] = useState("Content of the modal");
  const [modalTaskId, setModalTaskId] = useState("");

  const [dataCart, setDataCart] = useState([]);
  async function getDataCart() {
    try {
      await axios
        .get("http://localhost:3222/cart", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_customer")}`,
          },
        })
        .then((res) => {
          console.log(res.data.data);
          setDataCart(res.data.data);
        });
    } catch (error) {
      if (error) {
        message.error("error");
      }
    }
  }

  // const handleOkDelete = (id) => {
  //   axios
  //     .delete(`http://localhost:3222/cart/${id}`, {
  //       headers: {
  //         Authorization: `Bearer ${localStorage.getItem("token_customer")}`,
  //       },
  //     })
  //     .then((res) => {
  //       console.log(res, "berhasil");
  //     });
  // };

  // const handleCartDelete = () => {
  //   dataCart.map((data) => {
  //     handleOkDelete(data.id);
  //   });
  // };
  useEffect(() => {
    getDataCart();
  }, []);
  const handlePrint = () => {
    window.print();
  };
  return (
    <>
      <NavigasiPayment />
      <Row>
        <Col span={12}>
          <div className="flex flex-col pt-20 items-center h-screen ">
            <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8 ">
                <i
                  className="fa-regular fa-circle-check"
                  style={{
                    color: "#A6EEC7",
                    fontSize: "100px",
                  }}
                ></i>
              </div>
            </div>
            <div className="flex justify-center py-10">
              <div className="block p-6 rounded-lg shadow-lg bg-white max-w-sm">
                <h5 className="text-gray-900 text-xl leading-tight font-medium mb-2 text-center">
                  Paymnet Corfimed
                </h5>

                <p className="text-gray-700 text-sm  mb-4 text-center">
                  Thank you galuh for buying at Chikufarm. Once your order is
                  confirmed, it will be ready to ship within 1 hour.
                </p>
              </div>
            </div>
            <div>
              <button
                // onClick={handleCartDelete}
                type="button"
                className=" inline-block px-6 py-2.5 bg-[#56B280] text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-emerald-700 hover:shadow-lg focus:bg-emerald-700 focus:shadow-lg focus:outline-none focus:ring-0 active:shadow-lg transition duration-150 ease-in-out"
              >
                <a href="/landing" className="text-white">
                  Back to shoping
                </a>
              </button>
            </div>
            <div>
              <button
                type="button"
                onClick={handlePrint}
                className=" inline-block pt-5 text-[#56B280] font-medium text-md underline leading-tight transition duration-150 ease-in-out"
              >
                Print receipt
              </button>
            </div>
          </div>
        </Col>
        <Col span={12}>
          <Card style={{ backgroundColor: "#F2F2F2", height: "100%" }}>
            <Row className="border-b border-[#aaa]-300 mx-5 my-10"></Row>
            <Row className="mx-5">
              <Col span={6}>
                <p>Subtotal</p>
              </Col>
              <Col span={6} offset={12}>
                <h1>{dataCart[0]?.total_harga}</h1>
              </Col>
            </Row>
            <Row className="mx-5">
              <Col span={6}>
                <p>Shiping</p>
              </Col>
              <Col span={6} offset={12}>
                <p>free shiping</p>
              </Col>
            </Row>
            <Row className="border-b border-[#aaa]-300 mx-5 my-10"></Row>
            <Row className="mx-5">
              <Col span={6}>
                <p>Total</p>
              </Col>
              <Col span={6} offset={12}>
                <h1 className="text-xl">{dataCart[0]?.total_harga}</h1>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </>
  );
}
