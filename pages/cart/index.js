import { Card, Col, Row, Button } from "antd";
import React from "react";

import "antd/dist/antd.css";
import image1 from "../image/logo.svg";
import Image from "next/image";
import Link from "next/link";

const CardLogin = () => {
  return (
    <div className="site-card-border-less-wrapper">
      <Row justify="center" align="middle" style={{ height: "100vh" }}>
        <Col>
          <Card
            bordered={false}
            style={{
              width: 450,
              height: 300,
              margin: "auto",
              backgroundColor: "#56B280",
              borderRadius: "10px",
              justifyContent: "center",
              display: "flex",
            }}
          >
            <Image src={image1} width={"330%"} margin={"auto"} />

            <h1
              style={{
                fontSize: "16px",
                textAlign: "center",
                marginTop: "20px",
                marginBottom: "30px",
              }}
            >
              You Must Login!
            </h1>

            <div>
              <Button
                htmlType="submit"
                style={{
                  backgroundColor: "#F9EAE1",
                  width: 150,
                  height: 30,
                  borderRadius: 10,
                  borderColor: "#F9EAE1",
                  marginRight: 15,
                  color: "#76323f",
                  fontWeight: "bold",
                }}
                href="/"
              >
                Back
              </Button>
              <Button
                htmlType="submit"
                style={{
                  backgroundColor: "#F9EAE1",
                  width: 150,
                  height: 30,
                  borderRadius: 10,
                  borderColor: "#F9EAE1",
                  marginLeft: 15,
                  color: "#76323f",
                  fontWeight: "bold",
                }}
                href="/login"
              >
                LOGIN
              </Button>
            </div>
          </Card>
        </Col>
      </Row>
    </div>
  );
};
export default CardLogin;
