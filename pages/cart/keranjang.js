import NavbarCustomer from "../components/navbarCustomer";
import Footer from "../components/footer";
import "antd/dist/antd.css";
import "tailwindcss/tailwind.css";
import { Col, Row, Grid, Card, Table, Button, Space, Input, Tabs } from "antd";
import { Content } from "antd/lib/layout/layout";
import React, { useState, useEffect } from "react";

import OnGoing from "../components/onGoing";

import Link from "next/link";
import Image from "next/image";
import Finish from "../components/finish";
import MenungguConfirm from "../components/MenungguConfirm";

const tabList = [
  {
    key: "tab1",
    tab: "Ongoing",
  },
  {
    key: "tab2",
    tab: "Menunggu Konfirmasi",
  },
  {
    key: "tab3",
    tab: "Selesai",
  },
];
const contentList = {
  tab1: <OnGoing />,
  tab2: <MenungguConfirm />,
  tab3: <Finish />,
};

function Keranjang() {
  const [activeTabKey1, setActiveTabKey1] = useState("tab1");
  const onTab1Change = (key) => {
    setActiveTabKey1(key);
  };

  return (
    <>
      <NavbarCustomer />
      <div className="align-center h-screen pt-20">
        <div className="text-center pt-10">
          <h1 className=" text-xl ">Your Cart Items</h1>
          <p className="text-md mt-3 mb-5 underline ">
            <Link href="/">
              <a style={{ color: "green" }}>Back to Home</a>
            </Link>
          </p>
        </div>

        <Card
          style={{
            width: "85%",
            height: "60%",
            margin: "auto",
          }}
          tabList={tabList}
          activeTabKey={activeTabKey1}
          onTabChange={(key) => {
            onTab1Change(key);
          }}
        >
          {contentList[activeTabKey1]}
        </Card>
      </div>
      <Footer />
    </>
  );
}
export default Keranjang;
