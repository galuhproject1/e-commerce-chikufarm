import "tailwindcss/tailwind.css";
import "antd/dist/antd.css";
import FooterCustomer from "../components/footer";
import NavbarCustomer from "../components/navbarCustomer";
import NavigasiTiga from "../components/navbar3";
import Footer from "../components/footer";
import { useState, useEffect } from "react";

function ContactUs() {
  const [isLoged, setIsLoged] = useState(false);
  const getToken = () => {
    const token = localStorage.getItem("token_customer");
    if (token) {
      setIsLoged(true);
    } else {
      setIsLoged(false);
    }
  };
  useEffect(() => {
    getToken();
  }, []);

  return (
    <>
      {!isLoged ? <NavigasiTiga /> : <NavbarCustomer />}

      <div className="flex w-full flex-1 flex-col items-center justify-center text-center h-screen ">
        <div className=" p-6 rounded-lg shadow-xl outline-4 mt-5 outline-slate-600 max-w-md align-middle">
          <h1 className=" text-2xl">Contact Us</h1>
          <form>
            <div className="grid grid-cols-2 gap-4">
              <div className="form-group mb-6">
                <h1 className="text-start">First Name</h1>
                <input
                  type="text"
                  className="
                  form-control
                  block
                  w-full
                  px-3
                  py-1.5
                  text-base
                  font-normal
                  text-gray-700
                  bg-white bg-clip-padding
                  border border-solid border-gray-300
                  rounded
                  transition
                  ease-in-out
                  m-0
                  focus:text-gray-700 
                  focus:bg-white 
                  focus:border-blue-600 
                  focus:outline-none"
                  id="exampleInput123"
                  aria-describedby="emailHelp123"
                  placeholder="First name"
                />
              </div>
              <div className="form-group mb-6">
                <h1 className="text-start">Last Name</h1>
                <input
                  type="text"
                  className="
                  form-control
                  block
                  w-full
                  px-3
                  py-1.5
                  text-base
                  font-normal
                  text-gray-700
                  bg-white bg-clip-padding
                  border border-solid border-gray-300
                  rounded
                  transition
                  ease-in-out
                  m-0
                  focus:text-gray-700 
                  focus:bg-white 
                  focus:border-blue-600 
                  focus:outline-none"
                  id="exampleInput124"
                  aria-describedby="emailHelp124"
                  placeholder="Last name"
                />
              </div>
            </div>
            <div className="form-group mb-6">
              <h1 className="text-start">Email address</h1>
              <input
                type="email"
                className="
                form-control 
                block
                w-full
                px-3
                py-1.5
                text-base
                font-normal
                text-gray-700
                bg-white bg-clip-padding
                border border-solid border-gray-300
                rounded
                transition
                ease-in-out
                m-0
                focus:text-gray-700 
                focus:bg-white 
                focus:border-blue-600 
                focus:outline-none"
                id="exampleInput125"
                placeholder="Email address"
              />
            </div>
            <div className="form-group mb-6">
              <h1 className="text-start">Message</h1>
              <textarea
                className="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
                id="exampleFormControlTextarea13"
                rows="3"
                placeholder="Message"
              ></textarea>{" "}
            </div>
            <button
              type="submit"
              className="
            w-full
            px-6
            py-2.5
            bg-emerald-600
            text-white
            font-medium
            text-xs
            leading-tight
            uppercase
            rounded
            shadow-md
            transition
            duration-150
            ease-in-out"
            >
              SEND MESSAGE
            </button>
          </form>
        </div>
      </div>

      <Footer />
    </>
  );
}

export default ContactUs;
