import Image from "next/image";
import React from "react";
import Jumbo1 from "../image/ayam1.jpg";
import image2 from "../image/logoGambar.png";
import "tailwindcss/tailwind.css";
import { Col, Card, Row } from "antd";
import { useState, useEffect } from "react";
import NavigasiTiga from "../components/navbar3";
import Footer from "../components/footer";
import NavbarCustomer from "../components/navbarCustomer";
import cardImg1 from "../image/card.png";
import cardImg2 from "../image/misi2.png";

function AboutUs() {
  const [isLoged, setIsLoged] = useState(false);
  const getToken = () => {
    const token = localStorage.getItem("token_customer");
    if (token) {
      setIsLoged(true);
    } else {
      setIsLoged(false);
    }
  };
  useEffect(() => {
    getToken();
  }, []);
  return (
    <>
      {!isLoged ? <NavigasiTiga /> : <NavbarCustomer />}
      <div className="relative h-screen ">
        <Image src={Jumbo1} layout="fill" />
        <div className="flex justify-center absolute left-0 right-0 top-20 mt-20 pt-10">
          <div className="rounded-lg shadow-lg bg-white bg-opacity-80 max-w-md text-center">
            <div className="p-6">
              <Image src={image2} width={45} height={45} />
              <h5 className="text-gray-900 text-xl font-medium mb-2">
                Sejarah
              </h5>
              <p className="text-gray-700 text-base mb-4 text-justify font-semibold">
                Kami berdiri pada tahun 2022, dengan nama PT. Chikufarm yang
                telah menunjukan pendekatan yang berbeda untuk bisnis. Kami
                memulai dengan konsep inovatif berpadu dengan produk-produk
                berkualitas tinggi dan peluang bisnis yang dapat diikuti oleh
                orang awam sekaligus dengan system yang profitable.
              </p>
            </div>
          </div>
        </div>
      </div>

      <div className="flex w-full flex-col items-center justify-center mt-10">
        <div className=" rounded-lg bg-white max-w-2xl text-center">
          <div className="py-6 px-20">
            <h1 className="text-2xl underline underline-offset-8">VISI KAMI</h1>
            <p className="text-gray-700 text-base mb-4">
              Peternakan yang profesional, bermutu, dan berorientasi ketahanan
              pangan
            </p>
          </div>
        </div>
        <div className=" rounded-lg bg-white max-w-2xl text-center">
          <div className="p-6">
            <h1 className="text-2xl underline underline-offset-8">MISI KAMI</h1>
            {/* <p className="text-gray-700 text-base mb-4">
              Menjadikan Chikufarm sebagai peternakan yang memiliki standar mutu
              tinggi baik proses maupun hasilnya
            </p>
            <p>
              Menjadikan platfrom chikufarm sebagai solusi alternatif menjaga
              ketahanan pangan masyarakat
            </p>
            <p>Memberikan kemudahan pembayaran untuk kepuasan pelanggan</p>
            <p>
              Pengiriman produk cepat dan aman serta responsif terhadap segala
              bentuk perubahan yang membangun
            </p> */}
          </div>
        </div>
      </div>
      <Row className="my-5 justify-center">
        <Col
          lg={{ span: 8 }}
          md={{ span: 5 }}
          sm={{ span: 10 }}
          xs={{ span: 10 }}
          className="pt-5"
        >
          <div className="flex justify-center text-justify">
            <div className="flex flex-col md:flex-row md:max-w-xl rounded-lg bg-white shadow-lg">
              <Image
                className=" w-full h-96 md:h-auto object-cover md:w-48 rounded-t-lg md:rounded-none md:rounded-l-lg"
                src={cardImg1}
                alt=""
              />
              <div className="p-6 flex flex-col justify-start">
                <p className="text-gray-700 text-base mb-4">
                  Menjadikan Chikufarm sebagai peternakan yang memiliki standar
                  mutu tinggi baik proses maupun hasilnya
                </p>
              </div>
            </div>
          </div>
        </Col>
        <Col
          lg={{ span: 8, offset: 2 }}
          md={{ span: 5, offset: 2 }}
          sm={{ span: 10, offset: 2 }}
          xs={{ span: 10, offset: 2 }}
          className="pt-5"
        >
          <div className="flex justify-center text-justify">
            <div className="flex flex-col md:flex-row md:max-w-xl rounded-lg bg-white shadow-lg">
              <Image
                // className=" w-full h-96 md:h-auto object-cover md:w-48 rounded-t-lg md:rounded-none md:rounded-l-lg"
                src={cardImg2}
                alt=""
              />
              <div className="p-6 flex flex-col justify-start">
                <p className="text-gray-700 text-base mb-4">
                  Menjadikan platfrom chikufarm sebagai solusi alternatif
                  pemasaran dalam menjaga ketahanan pangan masyarakat
                </p>
              </div>
            </div>
          </div>
        </Col>
      </Row>

      <Footer />
    </>
  );
}

export default AboutUs;
