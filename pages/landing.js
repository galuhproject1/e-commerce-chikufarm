import Head from "next/head";
import Image from "next/image";
import NavigasiTiga from "./components/navbar3";
import styles from "../styles/Home.module.css";
import Header from "./components/header";
import "antd/dist/antd.css";
import ProductHome from "./components/product";
import OurService from "./components/ourService";
import MoreProduct from "./components/moreProduct";
import Footer from "./components/footer";
import NavbarCustomer from "./components/navbarCustomer";

export default function Home() {
  return (
    <>
      <NavbarCustomer />
      <Header />
      <ProductHome />
      <MoreProduct />
      <OurService />
      <Footer />
    </>
  );
}
