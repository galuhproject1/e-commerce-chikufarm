import {
  Layout,
  Header,
  Row,
  Col,
  Card,
  Space,
  Table,
  Tag,
  Button,
  Modal,
  Checkbox,
  Form,
  Input,
  Tooltip,
} from "antd";
import {
  PlusOutlined,
  PrinterOutlined,
  PoweroffOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import Link from "next/link";
import axios from "axios";
import { useRouter } from "next/router";
import jwt_decode from "jwt-decode";
const { Content } = Layout;
const { TextArea } = Input;

function getColumns(updateModal, deleteModal) {
  return [
    {
      title: "Produk",
      dataIndex: "nama",
      key: "nama",
    },
    // {
    //   title: "Gambar",
    //   dataIndex: "gambar",
    //   key: "gambar",
    // },
    {
      title: "Deskripsi",
      dataIndex: "deskripsi",
      key: "deskripsi",
    },
    {
      title: "Harga",
      dataIndex: "harga",
      key: "harga",
    },

    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Tooltip placement="left" title="edit">
            <Button
              onClick={() => updateModal(record)}
              style={{ color: "#517BEA", borderColor: "#4ade80" }}
              icon={<i className="fa-solid fa-pencil"></i>}
            ></Button>
          </Tooltip>

          <Tooltip placement="right" title="Delete">
            <Button
              type="danger"
              onClick={() => deleteModal(record)}
              icon={<i className="fa-solid fa-trash-can"></i>}
              danger={true}
            ></Button>
          </Tooltip>
        </Space>
      ),
    },
  ];
}

export default function KontenAgen() {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [loadingDua, setLoadingDua] = useState(false);

  // state modal delete
  const [visible, setVisible] = useState(false);
  const [modalText, setModalText] = useState("Content of the modal");
  const [modalTaskId, setModalTaskId] = useState("");

  //state modal update
  const [visibleDua, setVisibleDua] = useState(false);
  const [modalTextDua, setModalTextDua] = useState("Content of the modal");
  const [modalTaskIdDua, setModalTaskIdDua] = useState("");

  const handleCancel = () => {
    console.log("Clicked cancel button");
    setVisible(false);
    setVisibleDua(false);
    // setIsModalVisible(false);
  };

  // modal delete
  const deleteModal = (record) => {
    if (record) {
      setModalTaskId(record);
      setVisible(true);
    } else {
      setVisible(false);
    }
  };
  const handleOkModalDelete = () => {
    axios
      .delete(`http://localhost:3222/produk_pusat/${modalTaskId.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token_admin")}`,
        },
      })
      .then((res) => {
        console.log(res);
      });
    setModalText("The modal will be closed after one second");
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 1000);
    // location.reload()
  };

  // modal update
  const updateModal = (record) => {
    console.log(record, "ini dari Update Modal");
    if (record) {
      setModalTaskIdDua(record.id);
      setVisibleDua(true);
      form.setFieldsValue({
        nama: record.nama,
        harga: record.harga,
        deskripsi: record.deskripsi,
      });
    } else {
      setVisibleDua(false);
    }
  };

  const handleOkModalUpdate = async (record) => {
    // console.log(modalTaskIdDua, "ini record");
    try {
      const data = await form.getFieldsValue();
      console.log(data);
      await axios
        .put(`http://localhost:3222/produk_pusat/${modalTaskIdDua}`, data, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_admin")}`,
            "content-type": "application/json",
          },
        })
        .then((res) => {
          console.log(res, "Berhasil edit");
        });
      setModalTextDua("The modal will be closed after one seconds");
      setConfirmLoading(true);
      setTimeout(() => {
        setVisibleDua(false);
        setConfirmLoading(false);
      }, 1000);
      // location.reload();
    } catch (error) {
      console.log(error.message);
    }
  };

  const [dataProduk, setDataproduk] = useState([]);
  async function getData() {
    try {
      await axios
        .get("http://localhost:3222/produk_pusat", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_admin")}`,
          },
        })
        .then((res) => {
          setDataproduk(res.data.data);
        });
    } catch (error) {}
  }
  console.log(dataProduk);

  useEffect(() => {
    getData();
  }, []);

  // const onFinish = async (values) => {
  //   try {
  //     const response = await axios.post("http://localhost:3222/produk_pusat", {
  //       nama: values.fullname,
  //       alamat: values.alamat,
  //       email: values.email,
  //       password: values.password,
  //       no_hp: values.telepon,
  //       roles: role,
  //     });
  //     console.log(response);
  //   } catch (error) {}
  // };

  const temp = dataProduk.map((values) => {
    const data = {
      nama: values.nama_produk,
      id: values.id,
      deskripsi: values.deskripsi,
      harga: values.harga,
    };
    return data;
  });

  // const onFinish = (values) => {
  //   console.log("Success:", values);
  // };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  // const [isModalVisible, setIsModalVisible] = useState(false);

  // const showModal = () => {
  //   setIsModalVisible(true);
  // };

  // const handleOk = () => {
  //   setIsModalVisible(false);
  // };

  // const handleCancel = () => {
  //   setIsModalVisible(false);
  // };
  return (
    <div className="bg-white rounded-md align-middle my-10 mx-12 pb-5">
      {/* <Layout style={{ backgroundColor: "#A6EEC7" }}> */}
      <Content style={{ marginTop: "50px" }}>
        <Row justify="end">
          <Col span={22} pull={1} className="bg-white">
            <Row justify="end" className="mt-5 mr-5"></Row>
          </Col>
        </Row>

        <Row justify="center" align="middle">
          <Col span={22}>
            <Table
              columns={getColumns(updateModal, deleteModal)}
              pagination={false}
              dataSource={temp}
            />
            <Modal
              name="basic"
              title="Konfirmasi Penghapusan"
              visible={visible}
              onOk={handleOkModalDelete}
              confirmLoading={confirmLoading}
              onCancel={handleCancel}
            >
              <p className="text-emerald-500">
                Apakah anda yakin akan meghapus produk
              </p>
              <p className="text-emerald-500">
                {JSON.stringify(modalTaskId.nama)}
              </p>
            </Modal>
            <Modal
              title="Konfirmasi Update Data"
              visible={visibleDua}
              onOk={handleOkModalUpdate}
              confirmLoading={confirmLoading}
              onCancel={handleCancel}
              footer={false}
              // footer={false}
            >
              <Form
                form={form}
                name="basic"
                labelCol={{}}
                wrapperCol={{}}
                initialValues={{
                  remember: true,
                }}
                onFinish={handleOkModalUpdate}
                // onFinishFailed={onFinishFailed}
                // autoComplete="off"
              >
                <h1 className="text-start">Nama Produk</h1>
                <Form.Item name="nama">
                  <Input />
                </Form.Item>

                <h1 className="text-start">Deskripsi</h1>
                <Form.Item name="deskripsi">
                  <TextArea />
                </Form.Item>
                <h1 className="text-start">Harga</h1>
                <Form.Item name="harga">
                  <Input />
                </Form.Item>
                <Form.Item>
                  <Row>
                    <Col span={6} push={5} offset={3}>
                      <Button
                        type="primary"
                        htmlType="cancel"
                        onClick={handleCancel}
                      >
                        Cancel
                      </Button>
                    </Col>
                    <Col span={4} offset={4}>
                      <Button
                        type="primary"
                        htmlType="submit"
                        onClick={handleOkModalUpdate}
                      >
                        Submit
                      </Button>
                    </Col>
                  </Row>
                </Form.Item>
              </Form>
            </Modal>
          </Col>
        </Row>
      </Content>
      {/* </Layout> */}
    </div>
  );
}
