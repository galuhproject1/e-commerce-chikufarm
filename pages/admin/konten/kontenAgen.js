import {
  Layout,
  Header,
  Row,
  Col,
  Card,
  Space,
  Table,
  Tag,
  Button,
  Modal,
  Checkbox,
  Form,
  Input,
  Tooltip,
  Select,
  Option,
} from "antd";
import {
  PlusOutlined,
  PrinterOutlined,
  PoweroffOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import Link from "next/link";
import axios from "axios";
import jwt_decode from "jwt-decode";
import { useRouter } from "next/router";
import { data } from "autoprefixer";
const { Content } = Layout;

function getColumns(deleteModal, updateModal) {
  return [
    {
      title: "Full Name",
      dataIndex: "nama",
      key: "nama",
    },
    {
      title: "Alamat",
      dataIndex: "alamat",
      key: "alamat",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Phone",
      dataIndex: "no_hp",
      key: "no_hp",
    },

    {
      title: "Role",
      dataIndex: "",
      key: "role",
      render: () => <a>Agen</a>,
    },

    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Tooltip placement="left" title="edit">
            <Button
              onClick={() => updateModal(record)}
              style={{ color: "#517BEA", borderColor: "#4ade80" }}
              icon={<i className="fa-solid fa-pencil"></i>}
            ></Button>
          </Tooltip>

          <Tooltip placement="right" title="Delete">
            <Button
              type="danger"
              onClick={() => deleteModal(record)}
              icon={<i className="fa-solid fa-trash-can"></i>}
              danger={true}
            ></Button>
          </Tooltip>
        </Space>
      ),
    },
  ];
}

export default function KontenAgen() {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [loadingDua, setLoadingDua] = useState(false);
  // state modal delete
  const [visible, setVisible] = useState(false);
  const [modalText, setModalText] = useState("Content of the modal");
  const [modalTaskId, setModalTaskId] = useState("");
  //state modal update
  const [visibleDua, setVisibleDua] = useState(false);
  const [modalTextDua, setModalTextDua] = useState("Content of the modal");
  const [modalTaskIdDua, setModalTaskIdDua] = useState("");
  // const [foto, setFoto] = useState('')

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    console.log("Clicked cancel button");
    setVisible(false);
    setVisibleDua(false);
    setIsModalVisible(false);
  };
  // modal delete
  const deleteModal = (record) => {
    if (record) {
      setModalTaskId(record);
      setVisible(true);
    } else {
      setVisible(false);
    }
  };
  const handleOkModalDelete = () => {
    axios
      .delete(`http://localhost:3222/users/${modalTaskId.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token_admin")}`,
        },
      })
      .then((res) => {
        console.log(res);
      });
    setModalText("The modal will be closed after one second");
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 1000);
    // location.reload()
  };

  // modal update
  const updateModal = (record) => {
    console.log(record, "ini dari Update Modal");
    if (record) {
      setModalTaskIdDua(record.id);
      setVisibleDua(true);
      form.setFieldsValue({
        nama: record.nama,
        email: record.email,
        no_hp: record.no_hp,
        alamat: record.alamat,
      });
    } else {
      setVisibleDua(false);
    }
  };

  const handleOkModalUpdate = async (record) => {
    // console.log(modalTaskIdDua, "ini record");
    try {
      const data = await form.getFieldsValue();
      console.log(data);
      // const dataForm = new FormData();
      // dataForm.append("nama", data.nama);
      // dataForm.append("email", data.email);
      // dataForm.append("alamat", data.alamat);
      // dataForm.append("no_hp", data.no_hp);

      // console.log(data, "ini isi record");

      // for (let i = 0; i < data.variant.length; i++) {
      //   dataForm.append(`variant[${i}][name]`, data.variant[i].name);
      //   dataForm.append(`variant[${i}][price]`, data.variant[i].price);
      // }
      // for (const value of dataForm.values()) {
      //   console.log(value);
      // }

      await axios
        .put(`http://localhost:3222/users/${modalTaskIdDua}`, data, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_admin")}`,
            "content-type": "application/json",
          },
        })
        .then((res) => {
          console.log(res, "Berhasil edit");
        });
      setModalTextDua("The modal will be closed after one seconds");
      setConfirmLoading(true);
      setTimeout(() => {
        setVisibleDua(false);
        setConfirmLoading(false);
      }, 1000);
      // location.reload()
    } catch (error) {
      console.log(error.message);
    }
  };

  // get data ke tabel
  const [dataUser, setDatauser] = useState([]);
  async function getData() {
    try {
      await axios.get("http://localhost:3222/users?type=3").then((res) => {
        setDatauser(res.data.data);
      });
    } catch (error) {}
  }

  useEffect(() => {
    getData();
  }, []);

  const [email, setEmail] = useState("");
  const [nama, setNama] = useState("");
  const [password, setPassword] = useState("");
  const [no_hp, setNo_Hp] = useState("");
  const [alamat, setAlamat] = useState("");
  const [roles, setRoles] = useState("admin");

  const router = useRouter();

  const temp = dataUser.map((values) => {
    const data = {
      id: values.id,
      nama: values.nama,
      alamat: values.alamat,
      email: values.email,
      no_hp: values.no_hp,
    };
    return data;
  });

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const handlePrint = () => {
    window.print();
  };

  const { Option } = Select;

  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };
  const role = "agen";

  const onFinish = async (values) => {
    try {
      const response = await axios.post("http://localhost:3222/users", {
        nama: values.fullname,
        alamat: values.alamat,
        email: values.email,
        password: values.password,
        no_hp: values.telepon,
        roles: role,
      });
      console.log(response);
    } catch (error) {}
  };

  return (
    <div className="bg-white rounded-md align-middle my-10 mx-12 pb-5">
      {/* <Layout style={{ backgroundColor: "#A6EEC7" }}> */}
      <Content style={{ marginTop: "10px" }}>
        <Row justify="end">
          <Col span={22} pull={1} className="bg-white">
            <Row justify="end" className="mt-5 mr-5">
              <Col span={3} pull={2}>
                <Button
                  onClick={handlePrint}
                  type="primary"
                  style={{
                    marginBottom: "20px",
                    marginLeft: "90px",
                    marginBottom: "10px",
                  }}
                >
                  <PrinterOutlined />
                  Print
                </Button>
              </Col>
              <Col span={3} pull={2}>
                <Button
                  type="primary"
                  onClick={showModal}
                  style={{ marginBottom: "20px", marginLeft: "90px" }}
                >
                  <PlusOutlined />
                  Add User
                </Button>

                <Modal
                  title="Add User"
                  visible={isModalVisible}
                  onOk={handleOk}
                  onCancel={handleCancel}
                  footer={false}
                >
                  <Form
                    name="basic"
                    labelCol={{}}
                    wrapperCol={{}}
                    initialValues={{
                      remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                  >
                    <h1 className="text-start">Fullname</h1>
                    <Form.Item name="fullname">
                      <Input placeholder="Fullname" />
                    </Form.Item>

                    <h1 className="text-start">Alamat</h1>
                    <Form.Item name="alamat">
                      <Input placeholder="Alamat" />
                    </Form.Item>
                    <h1 className="text-start">Email</h1>
                    <Form.Item name="email">
                      <Input placeholder="Email" />
                    </Form.Item>
                    <h1 className="text-start">Password</h1>
                    <Form.Item name="password">
                      <Input.Password placeholder="Password" />
                    </Form.Item>
                    <h1 className="text-start">Phone</h1>
                    <Form.Item name="telepon">
                      <Input placeholder="Phone" />
                    </Form.Item>

                    <Form.Item>
                      <Row>
                        <Col span={6} push={5} offset={3}>
                          <Button
                            type="primary"
                            htmlType="cancel"
                            onClick={handleCancel}
                          >
                            Cancel
                          </Button>
                        </Col>
                        <Col span={4} offset={4}>
                          <Button
                            type="primary"
                            htmlType="submit"
                            onClick={handleOk}
                          >
                            Submit
                          </Button>
                        </Col>
                      </Row>
                    </Form.Item>
                  </Form>
                </Modal>
              </Col>
            </Row>
          </Col>
        </Row>

        <Row justify="center" align="middle">
          <Col span={22}>
            <Table
              columns={getColumns(deleteModal, updateModal)}
              dataSource={temp}
            />
            <Modal
              name="basic"
              title="Konfirmasi Penghapusan"
              visible={visible}
              onOk={handleOkModalDelete}
              confirmLoading={confirmLoading}
              onCancel={handleCancel}
            >
              <p className="text-emerald-500">
                Apakah anda yakin akan meghapus user yang memiliki nama
              </p>
              <p className="text-emerald-500">
                {JSON.stringify(modalTaskId.nama)}
              </p>
            </Modal>
            <Modal
              title="Konfirmasi Update Data"
              visible={visibleDua}
              onOk={handleOkModalUpdate}
              confirmLoading={confirmLoading}
              onCancel={handleCancel}
              // footer={false}
            >
              <Form
                form={form}
                name="basic"
                labelCol={{}}
                wrapperCol={{}}
                initialValues={{
                  remember: true,
                }}
                onFinish={handleOkModalUpdate}
                // onFinishFailed={onFinishFailed}
                // autoComplete="off"
              >
                <h1 className="text-start">Fullname</h1>
                <Form.Item name="nama">
                  <Input />
                </Form.Item>

                <h1 className="text-start">Alamat</h1>
                <Form.Item name="alamat">
                  <Input />
                </Form.Item>
                <h1 className="text-start">Email</h1>
                <Form.Item name="email">
                  <Input />
                </Form.Item>

                <h1 className="text-start">Phone</h1>
                <Form.Item name="no_hp">
                  <Input />
                </Form.Item>
                {/* <Form.Item>
                  <Row>
                    <Col span={6} push={5} offset={3}>
                      <Button
                        type="primary"
                        htmlType="cancel"
                        onClick={handleCancel}
                      >
                        Cancel
                      </Button>
                    </Col>
                    <Col span={4} offset={4}>
                      <Button
                        type="primary"
                        htmlType="submit"
                        onClick={handleOkModalUpdate}
                      >
                        Submit
                      </Button>
                    </Col>
                  </Row>
                </Form.Item> */}
              </Form>
            </Modal>
          </Col>
        </Row>
      </Content>
      {/* </Layout> */}
    </div>
  );
}
