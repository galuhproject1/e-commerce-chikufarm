import { Col, Row, Divider, Card, Button } from "antd";
import { PrinterOutlined } from "@ant-design/icons";
import { useState, useEffect } from "react";
import {
  AreaChart,
  Area,
  PieChart,
  Pie,
  Sector,
  Cell,
  ResponsiveContainer,
} from "recharts";
import axios from "axios";

// const totalProduk = data.length;
const data = [
  {
    name: "Juli",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Page F",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];

const data02 = [
  { name: "A1", value: 100 },
  { name: "A2", value: 300 },
  { name: "B1", value: 100 },
  { name: "B2", value: 80 },
  { name: "B3", value: 40 },
  { name: "B4", value: 30 },
  { name: "B5", value: 50 },
  { name: "C1", value: 100 },
  { name: "C2", value: 200 },
];

function KontenDashboard() {
  const [dataStok, setDataStok] = useState([]);
  async function getDataStok() {
    try {
      await axios
        .get("https://chikufarm-app.herokuapp.com/api/harvest", {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then((res) => {
          console.log(res.data);
          setDataStok(res.data);
        });
    } catch (error) {
      console.error(error);
    }
  }
  useEffect(() => {
    getDataStok();
  }, []);
  console.log(dataStok, "ini state");

  const COLORS = ["#76323F", "gray", "#DEC9B5"];
  return (
    <div className="mt-8">
      {dataStok.map((data) => {
        return (
          <Row justify="space-around" className="text-center">
            <Col
              span={4}
              offset={1}
              pull={1}
              className="bg-white p-5 rounded-xl"
            >
              <div className="text-sm text-black">Total Produk</div>
              <div className="text-lg font-bold text-bold">8</div>
            </Col>
            <Col
              span={4}
              offset={1}
              pull={1}
              className="bg-white p-5 rounded-xl"
            >
              <div className="text-sm text-black">Total Agen</div>
              <div className="text-lg font-bold text-bold">3</div>
            </Col>
            <Col
              span={4}
              offset={1}
              pull={1}
              className="bg-white p-5 rounded-xl"
            >
              <div className="text-sm text-black">Jumlah Pemasukan</div>
              <div className="text-lg font-bold text-bold">5.000.000</div>
            </Col>
            <Col
              span={4}
              offset={1}
              pull={1}
              className="bg-white p-5 rounded-xl"
            >
              <div className="text-sm text-black">Jumlah Stok</div>
              <div className="text-lg font-bold text-bold">
                {data?.population}
              </div>
            </Col>
            ;
          </Row>
        );
      })}
      <Row justify="space-around" className="text-center mt-5">
        <Col span={10} offset={1} pull={1}>
          <Card
            bordered={false}
            style={{
              backgroundColor: "white",
              borderRadius: "10px",
              height: "206px",
            }}
          >
            <div>progress Pembelian</div>
            <div>
              <AreaChart
                width={400}
                height={150}
                data={data}
                margin={{
                  top: 5,
                  right: 0,
                  left: 0,
                  bottom: 5,
                }}
              >
                <Area
                  type="monotone"
                  dataKey="uv"
                  stroke="#76323F"
                  fill="#A6EEC7"
                />
              </AreaChart>
            </div>
          </Card>
        </Col>

        <Col span={10} offset={1}>
          <Row gutter={[8, 24]}>
            <Col span={10} offset={1}>
              <Card
                bordered={false}
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  width: "96%",
                  height: "100%",
                }}
              >
                <div className="text-sm text-black">Penjualan Bulan Lalu</div>
                <div className="text-lg font-bold text-bold">300</div>
              </Card>
            </Col>
            <Col span={10} offset={1}>
              <Card
                bordered={false}
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  width: "96%",
                  height: "100%",
                }}
              >
                <div className="text-sm text-black">Penjualan Bulan Ini</div>
                <div className="text-lg font-bold text-bold">400</div>
              </Card>
            </Col>
            <Col span={21} offset={1}>
              <Card
                bordered={false}
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  width: "98%",
                  height: "100%",
                }}
              >
                <PieChart width={400} height={400}>
                  <Pie
                    data={data02}
                    dataKey="value"
                    cx="50%"
                    cy="50%"
                    innerRadius={70}
                    outerRadius={90}
                    fill="#82ca9d"
                    label
                  />
                  <Cell fill={COLORS[data02 % COLORS.length]} />
                </PieChart>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>

      <Row justify="space-around" className="mt-5">
        <Col span={10} offset={1} pull={1}>
          <Button
            style={{
              width: 450,
              height: 40,
              padding: "5px",
              backgroundColor: "#56B280",
              border: "10px",
              color: "white",
              fontSize: "20px",
            }}
          >
            Print Laporan <PrinterOutlined />
          </Button>
        </Col>
        <Col span={10} offset={1}></Col>
      </Row>
    </div>
  );
}

export default KontenDashboard;
