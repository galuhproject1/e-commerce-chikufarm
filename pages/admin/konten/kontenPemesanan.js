import {
  Layout,
  Header,
  Row,
  Col,
  Card,
  Space,
  Table,
  Tag,
  Button,
  Modal,
  Checkbox,
  Form,
  Input,
  Tooltip,
} from "antd";
import { PlusOutlined, EyeOutlined } from "@ant-design/icons";
import Image1 from "../../image/paymentconfirm.jpeg";
import React, { useState, useEffect } from "react";
import { render } from "react-dom";
import "antd/dist/antd.css";
import Link from "next/link";
import Image from "next/image";
import axios from "axios";
import { useRouter } from "next/router";
import jwt_decode from "jwt-decode";
const { Content } = Layout;

export default function KontenAgen() {
  const [token, setToken] = useState();
  const [dataPemesanan, setDatapemesanan] = useState([]);
  async function getData() {
    try {
      await axios
        .get("http://localhost:3222/transaksi_agen", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_admin")}`,
          },
        })
        .then((res) => {
          setDatapemesanan(res.data.data);
        });
    } catch (error) {}
  }

  useEffect(() => {
    getData();
  }, []);

  // const [isModalVisible, setIsModalVisible] = useState(false);

  // const showModal = () => {
  //   setIsModalVisible(true);
  // };

  // const handleOk = () => {
  //   setIsModalVisible(false);
  // };

  // const handleCancel1 = () => {
  //   setIsModalVisible(false);
  // };

  function getColumns(imageModal) {
    return [
      {
        title: "Nama",
        dataIndex: "agen",
        key: "agen",
      },
      {
        title: "Produk",
        dataIndex: "produk",
        key: "produk",
      },
      {
        title: "Jumlah Pembayaran",
        dataIndex: "total_bayar",
        key: "total_bayar",
      },
      {
        title: "Tanggal Pembayaran",
        dataIndex: "tanggal",
        key: "tanggal",
      },

      {
        title: "Bukti",
        dataIndex: "bukti_bayar",
        key: "bukti_bayar",
        render: (_, record) => {
          console.log(record);
          return (
            <>
              <Tooltip placement="left" title="Open Image">
                <Button
                  onClick={() => imageModal(record.bukti_bayar)}
                  style={{
                    border: "none",
                    overflow: "hidden",
                  }}
                  // icon={<EyeOutlined />}
                >
                  <p className="underline text-black">
                    Bukti <i className="fa-solid fa-eye underline"></i>
                  </p>
                </Button>
              </Tooltip>
            </>

            /* <Button
              type="primary"
              onClick={showModal}
              style={{
                overflow: "hidden",
                background: "white",
                borderColor: "white",
              }}
            >
              <p className="underline text-black">
                Bukti <i className="fa-solid fa-eye underline"></i>
              </p>
            </Button> */

            /* {
            <Modal
              title="Bukti Pembayaran"
              visible={isModalVisible}
              onOk={handleOk}
              onCancel={handleCancel}
              style={{ width: "200px", height: "200px" }}
            >
              <Image src={Image1} />
            </Modal>
          } */
          );
        },
      },
      {
        title: "Konfirmasi",
        key: "operation",
        fixed: "right",
        width: 100,
        render: () => (
          <a className="bg-emerald-500 px-5 py-1 rounded-md text-white">
            Confirm
          </a>
        ),
      },
    ];
  }

  // console.log(dataPemesanan[0].agen.nama);

  //state image modal

  const [visibleTiga, setVisibleTiga] = useState(false);
  const [modalTextTiga, setModalTextTiga] = useState("Content of the modal");
  const [modalTaskIdTiga, setModalTaskIdTiga] = useState("");
  const [loadingDua, setLoadingDua] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  let [imageUrl, setImageUrl] = useState("");

  const imageModal = async (record) => {
    setLoadingDua(true);
    if (record) {
      await setModalTaskIdTiga(record);
      setVisibleTiga(true);
      await axios
        .get(`http://localhost:3222/transaksi_agen/bukti_bayar/${record}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_admin")}`,
          },
        })
        .then((res) => {
          setImageUrl(res.config.url);
        });
    } else {
      setVisibleTiga(false);
    }
    setLoadingDua(false);
    console.log(modalTaskIdTiga);
  };
  // console.log(imageModal);

  const handleOkModalImage = () => {
    setImageUrl(null);
    setModalTextTiga("The modal will be closed after two seconds");
    setConfirmLoading(true);
    setTimeout(() => {
      setVisibleTiga(false);
      setConfirmLoading(false);
    }, 1000);
    // location.reload()
  };

  const handleCancel = () => {
    console.log("Clicked cancel button");
    // setVisible(false);
    setVisibleTiga(false);
  };

  const temp = dataPemesanan.map((values) => {
    // console.log(values);
    const data = {
      agen: values.agen.nama,
      total_bayar: values.total_bayar,
      produk: values.produkPusat.nama,
      tanggal: values.tanggal,
      bukti_bayar: values.bukti_bayar,
    };
    return data;
  });
  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="bg-white rounded-md align-middle my-10 mx-12 pb-5">
      <Content style={{ marginTop: "50px" }}>
        <Row justify="center" align="middle">
          <Col span={22}>
            <Table columns={getColumns(imageModal)} dataSource={temp} />
            <Modal
              title="Image"
              visible={visibleTiga}
              // onOk={handleOkModalImage}
              onCancel={handleCancel}
              confirmLoading={confirmLoading}
              footer={[
                <Button key="back" onClick={handleCancel}>
                  Return
                </Button>,
              ]}
            >
              <img src={imageUrl} width={500} height={500} />
            </Modal>
          </Col>
        </Row>
      </Content>
    </div>
  );
}
