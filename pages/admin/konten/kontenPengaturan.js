import "tailwindcss/tailwind.css";
import "antd/dist/antd.css";
import { AntDesignOutlined } from "@ant-design/icons";
import { Avatar, Col, Row } from "antd";
import React from "react";

const AvatarUser = () => (
  <Avatar
    size={{
      xs: 24,
      sm: 32,
      md: 40,
      lg: 64,
      xl: 80,
      xxl: 100,
    }}
    icon={<AntDesignOutlined />}
  />
);

function KontenPengaturan() {
  const edit = async (record) => {
    // console.log(modalTaskIdDua, "ini record");
    try {
      const data = await form.getFieldsValue();
      console.log(data);
      await axios
        .put(`http://localhost:3222/produk_pusat${modalTaskIdDua}`, data, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_admin")}`,
            "content-type": "application/json",
          },
        })
        .then((res) => {
          console.log(res, "Berhasil edit");
        });
      setModalTextDua("The modal will be closed after one seconds");
      setConfirmLoading(true);
      setTimeout(() => {
        setVisibleDua(false);
        setConfirmLoading(false);
      }, 1000);
      // location.reload();
    } catch (error) {
      console.log(error.message);
    }
  };
  return (
    <div className="my-10">
      <Row justify="center" align="middle" className="w-full text-center my-5">
        <Col
          span={22}
          className=" px-12 py-4 rounded-lg shadow-lg max-w-full mx-5 align-middle bg-white"
        >
          <Col className="my-2">
            <AvatarUser />
          </Col>
          <Col className="mb-5">
            <a className="bg-emerald-500 px-5 py-1 rounded-md text-white">
              Edit Avatar
            </a>
          </Col>
          <form>
            <div className="form-group mb-6">
              <h1 className="text-start">Fullname</h1>
              <input
                type="text"
                className="
                  form-control
                  block
                  w-full
                  px-3
                  py-1.5
                  text-base
                  font-normal
                  text-gray-700
                  bg-white bg-clip-padding
                  border border-solid border-gray-300
                  rounded
                  transition
                  ease-in-out
                  m-0
                  focus:text-gray-700 
                  focus:bg-white 
                  focus:border-blue-600 
                  focus:outline-none"
                id="exampleInput123"
                aria-describedby="emailHelp123"
                placeholder="Fullname"
              />
            </div>

            <div className="form-group mb-6">
              <h1 className="text-start">Email address</h1>
              <input
                type="email"
                className="
                form-control 
                block
                w-full
                px-3
                py-1.5
                text-base
                font-normal
                text-gray-700
                bg-white bg-clip-padding
                border border-solid border-gray-300
                rounded
                transition
                ease-in-out
                m-0
                focus:text-gray-700 
                focus:bg-white 
                focus:border-blue-600 
                focus:outline-none"
                id="exampleInput125"
                placeholder="Email"
              />
            </div>
            <div className="form-group mb-6">
              <h1 className="text-start">Nomor Telepon</h1>
              <input
                type="email"
                className="
                form-control 
                block
                w-full
                px-3
                py-1.5
                text-base
                font-normal
                text-gray-700
                bg-white bg-clip-padding
                border border-solid border-gray-300
                rounded
                transition
                ease-in-out
                m-0
                focus:text-gray-700 
                focus:bg-white 
                focus:border-blue-600 
                focus:outline-none"
                id="exampleInput125"
                placeholder="Email address"
              />
            </div>
            <div className="form-group mb-6">
              <h1 className="text-start">Password</h1>
              <input
                type="email"
                className="
                form-control 
                block
                w-full
                px-3
                py-1.5
                text-base
                font-normal
                text-gray-700
                bg-white bg-clip-padding
                border border-solid border-gray-300
                rounded
                transition
                ease-in-out
                m-0
                focus:text-gray-700 
                focus:bg-white 
                focus:border-blue-600 
                focus:outline-none"
                id="exampleInput125"
                placeholder="password"
              />
            </div>
            <div className="form-group mb-6">
              <h1 className="text-start">Confirm Password</h1>
              <input
                type="email"
                className="
                form-control 
                block
                w-full
                px-3
                py-1.5
                text-base
                font-normal
                text-gray-700
                bg-white bg-clip-padding
                border border-solid border-gray-300
                rounded
                transition
                ease-in-out
                m-0
                focus:text-gray-700 
                focus:bg-white 
                focus:border-blue-600 
                focus:outline-none"
                id="exampleInput125"
                placeholder="confirm password"
              />
            </div>
            <div className="form-group mb-6">
              <h1 className="text-start">Alamat</h1>
              <input
                type="email"
                className="
                form-control 
                block
                w-full
                px-3
                py-1.5
                text-base
                font-normal
                text-gray-700
                bg-white bg-clip-padding
                border border-solid border-gray-300
                rounded
                transition
                ease-in-out
                m-0
                focus:text-gray-700 
                focus:bg-white 
                focus:border-blue-600 
                focus:outline-none"
                id="exampleInput125"
                placeholder="alamat"
              />
            </div>
            <Row>
              <Col span={5}>
                <button
                  type="submit"
                  className="
            w-full
            px-6
            py-2.5
            bg-emerald-600
            text-white
            font-medium
            text-xs
            leading-tight
            uppercase
            rounded
            shadow-md
            transition
            duration-150
            ease-in-out"
                >
                  REMOVE ACCOUNT
                </button>
              </Col>
              <Col span={3} offset={12}>
                <button
                  type="submit"
                  className="
            w-full
            px-6
            py-2.5
            bg-white
            text-black
            font-medium
            text-xs
            leading-tight
            uppercase
            rounded
            shadow-md
            transition
            duration-150
            ease-in-out"
                >
                  CANCEL
                </button>
              </Col>
              <Col span={3} offset="1">
                <button
                  type="submit"
                  className="
            w-full
            px-6
            py-2.5
            bg-emerald-600
            text-white
            font-medium
            text-xs
            leading-tight
            uppercase
            rounded
            shadow-md
            transition
            duration-150
            ease-in-out"
                >
                  SAVE
                </button>
              </Col>
            </Row>
          </form>
        </Col>
      </Row>
    </div>
  );
}
export default KontenPengaturan;
