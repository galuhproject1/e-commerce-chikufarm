import KontenPengaturan from "./konten/kontenPengaturan";
import MainLayout from "./Layout/MainLayout";

function AgenAdmin() {
  return (
    <>
      <MainLayout>
        <KontenPengaturan />
      </MainLayout>
    </>
  );
}

export default AgenAdmin;
