import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined,
  SettingOutlined,
  DownOutlined,
} from "@ant-design/icons";
import Link from "next/link";
import {
  Button,
  Col,
  Layout,
  Menu,
  Row,
  Space,
  Dropdown,
  Avatar,
  message,
} from "antd";
import React, { useState } from "react";
import "antd/dist/antd.css";
import "tailwindcss/tailwind.css";
import Image from "next/image";
import logo from "../../image/logo.png";
import gambar from "../../image/logoGambar.png";
import { useRouter } from "next/router";
const { Header, Sider, Content } = Layout;

const AvatarAdmin = () => (
  <div>
    <Avatar
      shape="square"
      size="large"
      src={
        <Image
          src={gambar}
          style={{
            width: 32,
          }}
        />
      }
      className="border-2 border-grey"
    />
  </div>
);

const handleMenuClick = (e) => {
  message.info("Click on menu item.");
  console.log("click", e);
};
const handleButtonClick = (e) => {
  message.info("Click on left button.");
  console.log("click left button", e);
};

const NavbarAdmin = () => {
  const [collapsed, setCollapsed] = useState(false);
  const bgStyle = {
    backgroundColor: "white",
  };

  const handleLogout = () => {
    localStorage.removeItem("token_admin");
    router.push("/login");
  };

  const menu = (
    <Menu
      onClick={handleMenuClick}
      items={[
        {
          key: "1",
          label: (
            <>
              <h1 onClick={handleLogout}>
                <i className="fa-solid fa-right-from-bracket"></i> Logout
              </h1>
            </>
          ),
        },
      ]}
    />
  );
  const router = useRouter();

  return (
    <>
      <Header className="w-full text-xl" style={{ backgroundColor: "#A6EEC7" }}>
        <Row justify="end ">
          <Col
            className="gutter-row"
            span={4}
            style={{
              backgroundColor: "white",
              borderRadius: "10px",
              height: 54,
              width: 54,
              marginTop: 10,
            }}
          >
            <Row
              className="flex  text-center text-black space-x-2"
              style={{ height: 54 }}
            >
              <Col span={4} offset={1} className="-my-2">
                <AvatarAdmin />
              </Col>
              <Col span={8} offset={1} push={2} className="mt-3 text-lg">
                <p>admin</p>
              </Col>
              <Col span={4} offset={1} push={1} className="-my-2">
                <button
                  onClick={handleButtonClick}
                  href="jbuttonvascript:void(0)"
                >
                  <Dropdown overlay={menu}>
                    <a onClick={(e) => e.preventDefault()}>
                      <Space className="text-center ml-5 text-black text-lg">
                        <DownOutlined />
                      </Space>
                    </a>
                  </Dropdown>
                </button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Header>
    </>
  );
};

export default NavbarAdmin;
