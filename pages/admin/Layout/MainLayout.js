import { Layout, ConfigProvider, message } from "antd";
import NavbarAdmin from "./NavbarAdmin";
import Sidebar from "./SidebarAdmin";
import "antd/dist/antd.variable.css";
import "tailwindcss/tailwind.css";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
const { Footer } = Layout;
ConfigProvider.config({
  theme: {
    primaryColor: "#56B280",
  },
});

export default function MainLayout({ children }) {
  const router = useRouter();

  useEffect(() => {
    const getToken = localStorage.getItem("token_admin");
    if (!getToken) {
      message.error("Anda harus login dahulu");
      router.push("/login");
    }
  }, []);

  // const getTokenJwt = () => {
  //   try {
  //     const getToken = localStorage.getItem("access_token");
  //     const decodedToken = jwt_Decode(getToken);
  //     setUserName(decodedToken.name);
  //     console.log(decodedToken);
  //     if (decodedToken.role !== "Admin") {
  //       message.warn("You're Cannot Access This Page!");
  //       router.push("/login");
  //     }
  //   } catch (e) {
  //     console.log(e.message);
  //   }
  // };
  return (
    <Layout hasSider>
      <Sidebar />
      <Layout>
        <NavbarAdmin />
        <Layout style={{ backgroundColor: "#A6EEC7" }}>{children}</Layout>
      </Layout>
    </Layout>
  );
}
