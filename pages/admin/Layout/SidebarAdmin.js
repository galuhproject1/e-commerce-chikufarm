import "antd/dist/antd.css";
import "tailwindcss/tailwind.css";
import { Button, Layout, Menu, Divider } from "antd";
import { useState } from "react";
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined,
  DesktopOutlined,
  FileOutlined,
  TeamOutlined,
  StockOutlined,
  UnorderedListOutlined,
  SwapOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import Link from "next/link";
import Image from "next/image";
import logo from "../../image/logo.png";

const { Header, Sider, Content } = Layout;

export default function Sidebar() {
  const [collapsed, setCollapsed] = useState(false);
  const items = [
    {
      label: (
        <Link href="/admin/dashboard">
          <a className="w-full text-lg ">Dashboard</a>
        </Link>
      ),
      key: "dashboard",
      icon: <DesktopOutlined />,
      get: function getItem(label, key, icon, children) {
        return {
          key,
          icon,
          children,
          label,
        };
      },
    },
    {
      label: (
        <Link href="/admin/agen">
          <a className="w-full text-lg">Agen</a>
        </Link>
      ),
      key: "agen",
      icon: <TeamOutlined />,
      get: function getItem(label, key, icon, children) {
        return {
          key,
          icon,
          children,
          label,
        };
      },
    },
    {
      label: (
        <Link href="/admin/product">
          <a className="w-full text-lg ">Product</a>
        </Link>
      ),
      key: "product",
      icon: <StockOutlined />,
      get: function getItem(label, key, icon, children) {
        return {
          key,
          icon,
          children,
          label,
        };
      },
    },
    {
      label: (
        <Link href="/admin/pemesanan">
          <a className="w-full text-lg ">Pemesanan</a>
        </Link>
      ),
      key: "pemesanan",
      icon: <UnorderedListOutlined />,
      get: function getItem(label, key, icon, children) {
        return {
          key,
          icon,
          children,
          label,
        };
      },
    },
    {
      label: (
        <Link href="/admin/pengaturan">
          <a className="w-full text-lg ">Pengaturan</a>
        </Link>
      ),
      key: "pengaturan",
      icon: <SettingOutlined />,
      get: function getItem(label, key, icon, children) {
        return {
          key,
          icon,
          children,
          label,
        };
      },
    },
  ];

  return (
    <>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
        style={{
          overflow: "auto",
          minHeight: "100vh",
          maxHeight: "200vh",
        }}
        theme="light"
        className="drop-shadow-md"
      >
        <Image src={logo} layout="responsive" className="logo" />
        <Menu
          theme="light"
          mode="inline"
          defaultSelectedKeys={["1"]}
          items={items}
        />
      </Sider>
    </>
  );
}
