import KontenAgen from "./konten/kontenAgen";
import MainLayout from "./Layout/MainLayout";

function AgenAdmin() {
  return (
    <>
      <MainLayout>
        <KontenAgen />
      </MainLayout>
    </>
  );
}

export default AgenAdmin;
