import Footer from "../components/footer";
import NavbarCustomer from "../components/navbarCustomer";
import "tailwindcss/tailwind.css";
import "antd/dist/antd.css";
import { Card, Button, Form, Input, Row } from "antd";
import { IdcardOutlined, LockOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";

export default function Password() {
  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <>
      <NavbarCustomer />
      <Card className="">
        <div className="p-4 mb-4 ml-10 text-lg font-bold text-textColor mt-10">
          Account Setting
        </div>

        <div className="p-10 bg-white rounded-xl">
          <div className="grid grid-cols-4 gap-4">
            <Row className="">
              <Button
                className="mr-4 font-semibold text-textColor w-6/12 mb-5"
                href="/customer/pengaturan"
              >
                <IdcardOutlined className="mr-4 text-xl text-center " />
                Account
              </Button>
              <Button
                className="mr-4 font-semibold text-textColor w-6/12 -mt-20"
                href="/customer/password"
              >
                <LockOutlined className="mr-4 text-xl text-center" />
                Password
              </Button>
            </Row>
            <Row className="p-5 ml-5 border-l border-shadowColor">
              <div className="ml-5 mb-7 text-lg font-bold text-textColor">
                Password
              </div>
              {/* <div className="my-4 ml-6"> */}
              {/* {checkImage()}
                <UploadImage onChangeImage={handleChangeImage} /> */}
              {/* </div> */}

              <Row className="grid grid-cols-4 gap-4  ml-6">
                <Form
                  name="basic"
                  labelCol={{ span: 8 }}
                  wrapperCol={{ span: 20 }}
                  initialValues={{ remember: true }}
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                  autoComplete="off"
                  style={{ width: "450px" }}
                >
                  <h1 forInput="fullname">Password</h1>
                  <Form.Item name="nama">
                    <Input placeholder="Fullname" />
                  </Form.Item>

                  <h1 forInput="alamat">Confirm Password</h1>
                  <Form.Item name="alamat">
                    <Input placeholder="alamat" />
                  </Form.Item>

                  <div className="col-span-2 mt-5">
                    <Button
                      className="text-sm font-semibold rounded-l  border-black "
                      key="submit"
                      type="submit"
                      // onClick={onFinish}
                      style={{
                        background: "#56B280",
                        border: "solid 1px",
                        width: "377px",
                        borderColor: "#eee",
                      }}
                    >
                      Save Change
                    </Button>
                  </div>
                </Form>
              </Row>
            </Row>
          </div>
        </div>
      </Card>
      <Footer />
    </>
  );
}
