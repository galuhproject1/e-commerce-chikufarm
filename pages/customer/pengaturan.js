import "tailwindcss/tailwind.css";
import "antd/dist/antd.css";
import NavbarCustomer from "../components/navbarCustomer";
import Footer from "../components/footer";
import { IdcardOutlined, LockOutlined } from "@ant-design/icons";
import { Button, Card, Form, Input, Row, Col } from "antd";
import { useState, useEffect } from "react";
import axios from "axios";
import jwtDecode from "jwt-decode";
import Link from "next/link";

export default function pengaturan() {
  const [formUser] = Form.useForm();
  const [nama, setNama] = useState([]);
  const [alamat, setAlamat] = useState([]);
  const [no_hp, setNo_Hp] = useState([]);
  const [email, setEmail] = useState([]);

  // const handleChangeImage = (filepath) => {
  //   setPathImage(filepath);
  // };

  // const checkImage = () => {
  //   if (pathImage.length !== 0) {
  //     return (
  //       <Image
  //         className="rounded-full bg-cream"
  //         loader={() => pathImage}
  //         priority={true}
  //         unoptimized={true}
  //         src={`https://chikufarm-app.herokuapp.com/api/users/profile-picture/${pathImage}`}
  //         width={120}
  //         height={120}
  //         alt=""
  //       />
  //     );
  //   } else {
  //     return (
  //       <Image
  //         className="rounded-full bg-cream"
  //         loader={() => dataUser.profilePicture}
  //         priority={true}
  //         unoptimized={true}
  //         src={`https://chikufarm-app.herokuapp.com/api/users/profile-picture/${dataUser.profilePicture}`}
  //         width={120}
  //         height={120}
  //         alt=""
  //       />
  //     );
  //   }
  // };

  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const getTokenJwt = async () => {
    try {
      const getToken = localStorage.getItem("token_customer");
      const decode = await jwtDecode(getToken);
      const response = await axios.put(
        `http://localhost:3222/users/${decode.id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token_customer")}`,
          },
        }
      );
      console.log(response.data.data, "ini dari response");
      setNama(response.data.data.nama);
      setAlamat(response.data.data.alamat);
      setNo_Hp(response.data.data.no_hp);
      setEmail(response.data.data.email);
    } catch (e) {
      console.log(e.message);
    }
  };
  useEffect(() => {
    getTokenJwt();
  }, []);
  formUser.setFieldsValue({
    nama: nama,
    alamat: alamat,
    no_hp: no_hp,
    email: email,
  });
  return (
    <>
      <NavbarCustomer />
      <Card className="">
        <div className="p-4 mb-4 ml-10 text-lg font-bold text-textColor mt-10">
          Account Setting
        </div>

        <div className="p-10 bg-white rounded-xl">
          <div className="grid grid-cols-4 gap-4">
            <Row className="grid h-48 grid-rows-1">
              <Button
                className="mr-4 font-semibold text-textColor w-6/12"
                href="/customer/pengaturan"
              >
                <IdcardOutlined className="mr-4 text-xl text-center " />
                Account
              </Button>
              <Button
                className="mr-4 font-semibold text-textColor w-6/12"
                href="/customer/password"
              >
                <LockOutlined className="mr-4 text-xl text-center" />
                Password
              </Button>
            </Row>
            <Row className="p-5 ml-5 border-l border-shadowColor">
              <div className="ml-5 mb-7 text-lg font-bold text-textColor">
                General Info
              </div>
              {/* <div className="my-4 ml-6"> */}
              {/* {checkImage()}
                <UploadImage onChangeImage={handleChangeImage} /> */}
              {/* </div> */}

              <Row className="grid grid-cols-4 gap-4  ml-6">
                <Form
                  form={formUser}
                  name="basic"
                  labelCol={{ span: 8 }}
                  wrapperCol={{ span: 20 }}
                  initialValues={{ remember: true }}
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                  autoComplete="off"
                  style={{ width: "450px" }}
                >
                  <h1 forInput="fullname">Nama lengkap</h1>
                  <Form.Item name="nama">
                    <Input placeholder="Fullname" />
                  </Form.Item>

                  <h1 forInput="alamat">Alamat</h1>
                  <Form.Item name="alamat">
                    <Input placeholder="alamat" />
                  </Form.Item>

                  <h1 forInput="email">Email</h1>
                  <Form.Item name="email">
                    <Input placeholder="email" />
                  </Form.Item>

                  <h1 forInput="phone">No hp</h1>
                  <Form.Item name="no_hp">
                    <Input placeholder="Fullname" />
                  </Form.Item>

                  <div className="col-span-2 mt-5">
                    <Button
                      className="text-sm font-semibold rounded-l  border-black "
                      key="submit"
                      type="submit"
                      // onClick={onFinish}
                      style={{
                        background: "#56B280",
                        border: "solid 1px",
                        width: "377px",
                        borderColor: "#eee",
                      }}
                    >
                      Save Change
                    </Button>
                  </div>
                </Form>
              </Row>
            </Row>
          </div>
        </div>
      </Card>

      <Footer />
    </>
  );
}
