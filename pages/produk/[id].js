import "antd/dist/antd.css";
import "tailwindcss/tailwind.css";
import Link from "next/link";
import Image from "next/image";
import Footer from "../components/footer";
import NavigasiTiga from "../components/navbar3";
import NavbarCustomer from "../components/navbarCustomer";
import MenuSatu from "../image/bibit-ayam 1.png";
import menuDua from "../image/telur-ayam 1.png";
import { useEffect, useState } from "react";
import { Row, Col, Space, message } from "antd";
import { ShoppingCartOutlined } from "@ant-design/icons";
import { useRouter, Router } from "next/router";
import axios from "axios";
import jwtDecode from "jwt-decode";

export default function detail() {
  const [isLoged, setIsLoged] = useState(false);
  const [product, setProduct] = useState([]);
  const [name, setName] = useState([]);
  const [nameProduct, setNameProduct] = useState("");
  const [hargaProduk, setHargaProduk] = useState(0);
  // const onChange = (value) => {
  //   console.log("changed", value);
  // };

  const getToken = () => {
    const token = localStorage.getItem("token_customer");
    if (token) {
      setIsLoged(true);
    } else {
      setIsLoged(false);
    }
  };

  const handleCart = () => {
    if (isLoged) {
      // alert("Please Login");
      router.push("/cart");
    } else {
      tambahCart();
    }
  };

  const [dataProduct, setDataProduct] = useState([]);
  const router = useRouter();
  const { id } = router.query;

  async function getData() {
    try {
      const getToken = localStorage.getItem("token_customer");
      const decode = jwtDecode(getToken);
      const Nama = decode.name;
      setName(Nama);
      // console.log(decode.name);
      axios
        .get(`http://localhost:3222/produk_agen/produk/${id}`)
        .then((res) => {
          console.log(res);
          setProduct(res.data.data);
          setNameProduct(res.data.data.nama_produk);
          setHargaProduk(res.data.data.harga);
          setDataProduct([res.data.data]);
        });
    } catch (error) {}
  }

  useEffect(() => {
    getData();
  }, []);

  //post cart
  // const [nama_produk, setNama_Produk] = useState("");
  // const [nama_pembeli, setNama_Pembeli] = useState("");
  const [jumlah_produk, setJumlah_Produk] = useState(0);
  // const [harga_produk, setHarga_Produk] = useState("");

  const tambahCart = async () => {
    const jml = Number(jumlah_produk);
    const valueFrom = {
      email: name,
      nama_produk: nameProduct,
      jumlah_produk: jml,
      harga_produk: hargaProduk,
    };
    const res = await axios.post("http://localhost:3222/cart", valueFrom, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token_customer")}`,
        "content-type": "application/json",
      },
    });
    console.log(res);
    if (res.status == 201 || res.status == 200) {
      router.push("/cart/keranjang");
    } else {
      alert("error");
    }
  };

  const onChangeNama_Pembeli = (e) => {
    const value = e.target.value;
    setNama_Pembeli(value);
  };
  const onChangeNama_Produk = (e) => {
    const value = e.target.value;
    setNama_Produk(value);
  };
  const onChangeJumlah_Produk = (e) => {
    const value = e.target.value;
    setJumlah_Produk(value);
  };
  const onChangeTotal_Harga = (e) => {
    const value = e.target.value;
    setTotal_Harga(value);
  };

  const onFormSubmit = (e) => {
    e.preventDefault();
    console.log("ok");
  };
  return (
    <>
      <NavbarCustomer />
      <div className=" h-screen ml-40 pt-24">
        <Row justify="center">
          {dataProduct.map((data) => {
            // console.log(data.foto, "data uy");
            return (
              <>
                <Col span="10" pull={2}>
                  <Image
                    src={`http://localhost:3222/produk_agen/${data.foto}`}
                    alt="gambar"
                    height={350}
                    width={350}
                    data
                  />
                  <p className="text-base text-center font-semibold mt-2 pr-20 mr-4">
                    All processes with optimal effort, Chikufarm is there to
                    meet your needs.
                  </p>
                </Col>

                <Col
                  style={{ textAlign: "start", marginLeft: 20 }}
                  span="10"
                  pull={2}
                  data
                  key={product}
                >
                  <h2 className="font-bold text-2xl text-black">
                    {product.nama_produk}
                  </h2>
                  <h2 className="font-semibold text-xl my-3 text-black">
                    {product.harga}
                  </h2>
                  <p>{product.deskripsi}</p>
                  <div className="flex justify-center mt-8">
                    <div className="mt-20 mb-5 xl:w-20">
                      <label
                        htmlFor="exampleNumber0"
                        className="ml-4  text-black font-semibold"
                      ></label>
                      <input type="hidden" value={name} />
                      <input type="hidden" value={nameProduct} />
                      <input type="hidden" value={hargaProduk} />
                      <input
                        type="number"
                        onChange={onChangeJumlah_Produk}
                        className="
                                        form-control
                                        block
                                        w-full
                                        px-2
                                        py-0.5
                                       
                                        text-base
                                        font-normal
                                        text-gray-700
                                        bg-white bg-clip-padding
                                        border border-solid border-black
                                     
                                        transition
                                        ease-in-out
                                        m-0
                                        ml-2
                                        focus:text-gray-700 focus:bg-white focus:border-[#C78342] focus:outline-[#C78342]
                                        "
                        id="exampleNumber0"
                        placeholder="0"
                        style={{
                          width: "100px",
                        }}
                      />
                    </div>
                  </div>

                  <button
                    onClick={handleCart}
                    type="button"
                    style={{
                      width: "480px",
                    }}
                    className=" space-x-2 justify-end inline-block px-6 bg-[#56B280] text-white font-medium text-xs leading-tight shadow-md focus:shadow-lg hover:text-white hover:bg-[#56B280] active:bg-[#56B280]"
                  >
                    {<ShoppingCartOutlined className="mr-2 mb-2 text-xl " />}
                    <Space className="text-sm mt-2">+ Add to cart</Space>
                  </button>
                </Col>
              </>
            );
          })}
        </Row>
      </div>
      <Footer />
    </>
  );
}
